﻿using UnityEngine;
using System.Collections;

namespace Athena.Common
{
    public interface TransitionDelegate
    {
        IEnumerator Execute();
    }

    public class SceneTransitionSequential : TransitionDelegate
    {
        public SceneLoader sceneLoader;
        public System.Action onOldSceneOut;
        public System.Action onOldSceneOutFinished;
        public System.Action onNewSceneIn;
        public System.Action onNewSceneInFinished;

        public virtual IEnumerator Execute()
        {
            Enter();

            if (sceneLoader != null)
            {
                sceneLoader.PreLoad();
            }

            onOldSceneOut?.Invoke();

            while (!StepOut(Time.deltaTime))
                yield return null;

            onOldSceneOutFinished?.Invoke();

            if (sceneLoader != null)
            {
                sceneLoader.PreLoadDone();
                sceneLoader.Load();

                while (!sceneLoader.IsFinished())
                {
                    StepLoad(Time.deltaTime);
                    yield return null;
                }

                sceneLoader.ActiveScene();
            }

            onNewSceneIn?.Invoke();

            while (!StepIn(Time.deltaTime))
                yield return null;

            onNewSceneInFinished?.Invoke();

            End();
        }

        public virtual void StepLoad(float dt)
        {

        }

        public virtual bool StepOut(float dt)
        {
            return true;
        }

        public virtual bool StepIn(float dt)
        {
            return true;
        }

        public virtual void Enter()
        {

        }

        public virtual void End()
        {

        }
    }

    public class SceneTransitionConcurrent : TransitionDelegate
    {
        public SceneLoader sceneLoader;

        public IEnumerator Execute()
        {
            Enter();

            if (sceneLoader != null)
            {
                sceneLoader.Load();

                yield return null;

                sceneLoader.ActiveScene();

                while (!sceneLoader.IsFinished())
                {
                    StepLoad(Time.deltaTime);
                    yield return null;
                }
            }

            while (!Step(Time.deltaTime))
                yield return null;

            End();
        }

        public virtual void StepLoad(float dt)
        {

        }

        public virtual bool Step(float dt)
        {
            return true;
        }

        public virtual void Enter()
        {

        }

        public virtual void End()
        {

        }
    }
}