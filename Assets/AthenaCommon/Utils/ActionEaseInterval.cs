﻿using UnityEngine;

namespace Athena.Common.Utils
{
    public class ActionEaseInterval
    {
        public ActionEaseInterval() { }
        public ActionEaseInterval(float duration, TweenFunc.TweenType tweenType = TweenFunc.TweenType.Linear)
        {
            this.duration = duration;
            this.tweenType = tweenType;
        }

        public float duration;
        public TweenFunc.TweenType tweenType;
        public float[] easingParam;

        public bool IsFinished => (elapsed >= duration);
        public float elapsed { get; private set; }

        protected bool _firstTick = true;

        public void Finish(bool lastTick = false)
        {
            elapsed = duration;
            if (lastTick)
            {
                elapsed -= Time.deltaTime;
            }
        }

        public void Reset()
        {
            _firstTick = true;
            elapsed = 0f;
        }

        public float Step(float dt)
        {
            if (_firstTick)
            {
                _firstTick = false;
                elapsed = 0;
            }
            else
            {
                elapsed += dt;
            }

            float t = Mathf.Max(0f, Mathf.Min(1f, elapsed / Mathf.Max(duration, Mathf.Epsilon)));
            t = TweenFunc.tweenTo(t, tweenType, easingParam);
            return t;
        }

        public float Step()
        {
            return Step(Time.deltaTime);
        }
    }
}
