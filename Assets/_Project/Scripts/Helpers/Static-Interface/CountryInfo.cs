using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class CountryInfo
{
    private static SortedSet<string> _countryNames;

    public static IReadOnlyCollection<string> CountryNames
    {
        get
        {
            if (_countryNames == null) { Init(); }
            return _countryNames;
        }
    }

    private static void Init()
    {
        var countryNames = new SortedSet<string>();

        var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

        foreach (var cultureInfo in cultures)
        {
            var regionInfo = new RegionInfo(cultureInfo.Name);
            var countryName = regionInfo.EnglishName;

            // e.g. Hong Kong SAR China
            var SAR = " SAR";
            if (countryName.Contains(SAR))
            {
                countryName = countryName.Split(SAR)[0];
            }
            countryNames.Add(countryName);
        }

        _countryNames = countryNames;
    }
}
