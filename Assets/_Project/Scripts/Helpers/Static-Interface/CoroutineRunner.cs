using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall;
using UnityEngine;

public class CoroutineRunner
{
    protected MonoBehaviour _monoBehaviour;

    protected CoroutineRunner() { }

    public CoroutineRunner(MonoBehaviour monoBehaviour)
    {
        _monoBehaviour = monoBehaviour;
    }

    public Coroutine WaitAndDo(float delay, Action callback)
    {
        return StartCoroutine(WaitAndDoRoutine());
        IEnumerator WaitAndDoRoutine()
        {
            yield return YieldCollection.WaitForSeconds(delay);
            callback?.Invoke();
        }
    }

    public Coroutine StartCoroutine(IEnumerator routine)
    {
        if (routine == null) { return null; }
        return _monoBehaviour.StartCoroutine(routine);
    }

    public void StopCoroutine(Coroutine coroutine)
    {
        if (coroutine != null)
        {
            _monoBehaviour.StopCoroutine(coroutine);
        }
    }

    public void StopAllCoroutines()
    {
        _monoBehaviour.StopAllCoroutines();
    }
}
