using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCharacter
{
    public const char Tab = '\t';
    public const char Return = '\r';
    public const char NewLine = '\n';
    public const char Backspace = '\b';
    public const char EndOfText = (char)3;
}
