using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall
{
    public class CollectionUtils
    {
        public static List<T> Join<T>(params ICollection<T>[] collections)
        {
            var list = new List<T>();
            foreach (var collection in collections)
            {
                list.AddRange(collection);
            }
            return list;
        }
    }
}
