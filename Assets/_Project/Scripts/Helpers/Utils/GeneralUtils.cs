﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public static class GeneralUtils
{
    public static Vector3 GetCenterPos(IEnumerable<Vector3> enumerable)
    {
        var pos = Vector3.zero;
        var count = 0;

        foreach (var item in enumerable)
        {
            pos += item;
            count++;
        }

        return pos / count;
    }

    public static Vector3 GetCenterPos(params Vector3[] array)
    {
        return GetCenterPos((IEnumerable<Vector3>)array);
    }

    public static string GenerateRandomName(string prefix = "Player", string randomCharacters = "0123456789", int randomLength = 5)
    {
        var charArray = randomCharacters.ToCharArray();

        var stringBuilder = new StringBuilder();
        stringBuilder.Append(prefix);

        for (int i = 0; i < randomLength; i++)
        {
            var randomChar = charArray.GetRandom();
            stringBuilder.Append(randomChar);
        }

        return stringBuilder.ToString();
    }

    public static string DictToJson(Dictionary<string, string> dict)
    {
        var json = "{" + dict.JoinElements("\"{0}\":\"{1}\"", ",") + "}";
        return json;
    }

    public static SpriteState CreateIdenticalSpriteState(Sprite sprite)
    {
        var spriteState = new SpriteState
        {
            highlightedSprite = sprite,
            pressedSprite = sprite,
            selectedSprite = sprite,
            disabledSprite = sprite
        };
        return spriteState;
    }

    public static T[] GetEnums<T>() where T : Enum
    {
        var array = (T[])Enum.GetValues(typeof(T));
        return array;
    }

    public static void SimulateClick(GameObject target)
    {
        var pointerEventData = new PointerEventData(EventSystem.current);
        ExecuteEvents.Execute(target, pointerEventData, ExecuteEvents.pointerClickHandler);
        ExecuteEvents.Execute(target, pointerEventData, ExecuteEvents.submitHandler);
    }

    public static bool ClickedInsideRect(RectTransform rectTransform, Camera cam = null)
    {
        if (Input.GetMouseButtonDown(0))
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, Input.mousePosition, cam ?? Camera.main, out var localPoint);
            return rectTransform.rect.Contains(localPoint);
        }

        return false;
    }

    /// <summary> Get a random index from an array of chances based on its values </summary>
    public static int GetIndexFromLootTable(params int[] chances)
    {
        if (chances.Length == 0)
        {
            Debug.LogError("Loot Table has 0 element!");
            return 0;
        }

        if (chances.Length == 1) { return 0; }

        int tableLength = chances.Length;

        int total = 0;

        for (int i = 0; i < tableLength; i++)
        {
            total += chances[i];
        }

        var randomChance = UnityEngine.Random.Range(0, total + 1);

        for (var i = 0; i < tableLength; i++)
        {
            if (chances[i] <= 0) { continue; }
            if (randomChance <= chances[i])
            {
                return i;
            }
            else
            {
                randomChance -= chances[i];
            }
        }
        Debug.LogWarning("Exit Loot Table");
        return 0;
    }

    public static string GetMethodCallerInfo(int frameSkip = 0)
    {
        var stackTrace = new System.Diagnostics.StackTrace(true);
        var frame = stackTrace.GetFrame(2 + frameSkip); // 0 is this method, 1 is this method's caller, 2 is the upper method

        var methodName = frame.GetMethod().Name.Replace("b__0", "").Replace("<", "").Replace(">", "");
        var fileName = System.IO.Path.GetFileName(frame.GetFileName());
        var lineNumber = frame.GetFileLineNumber().ToString();
        var output = $"{methodName.Colorize(Color.yellow)} in {fileName.Colorize(Color.yellow)} at line {lineNumber.Colorize(Color.yellow)}";

        return output;
    }

    public static bool RandomTrue()
    {
        return UnityEngine.Random.value > 0.5f;
    }

}