using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextureUtils
{
    // Textures are not readable/writeable by default, copy will return a modifiable texture
    public static Texture2D Copy(Texture2D source)
    {
        var dest = new Texture2D(source.width, source.height, source.format, false);
        Graphics.CopyTexture(source, dest);
        return dest;
    }

}