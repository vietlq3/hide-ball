using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public static class ValidateUtils
{
    public static bool IsValidPIN(string pin)
    {
        if (pin.IsNullOrEmpty()) { return false; }

        if (pin.Length != 4 || pin.Length != 6)
        {
            return false;
        }

        foreach (char c in pin)
        {
            if (c < '0' || c > '9')
            {
                return false;
            }
        }
        return true;
    }

    public static bool IsValidEmail(string email)
    {
        try
        {
            var emailAddress = new System.Net.Mail.MailAddress(email);
            return true;
        }
        catch
        {
            return false;
        }
    }

    // https://stackoverflow.com/a/20971688
    public static bool IsValidPhoneNumber(string phoneNumber)
    {
        if (phoneNumber.IsNullOrEmpty()) { return false; }
        string regex = @"^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d*)\)?)[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$";
        try
        {
            var isMatch = Regex.IsMatch(phoneNumber, regex, RegexOptions.IgnoreCase | RegexOptions.Multiline, TimeSpan.FromMilliseconds(500));
            return isMatch;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            return false;
        }
    }

    // https://www.twilio.com/docs/glossary/what-e164
    public static bool IsValidPhoneNumberE164(string phoneNumber)
    {
        if (phoneNumber.IsNullOrEmpty()) { return false; }
        if (!phoneNumber.StartsWith('+')) { phoneNumber = "+" + phoneNumber; }
        if (phoneNumber.Length < 8) { return false; } // Shortest phone number contains 7 digits with + sign
        string regex = @"^\+[1-9]\d{1,14}$";
        try
        {
            var isMatch = Regex.IsMatch(phoneNumber, regex, RegexOptions.IgnoreCase | RegexOptions.Multiline, TimeSpan.FromMilliseconds(500));
            return isMatch;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
            return false;
        }
    }
}