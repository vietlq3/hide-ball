using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Seekers.HideBall;
using Seekers.HideBall.UI;

public class CoroutineUtils
{
    /// <summary> 1d 23h or 23h 45m or 3m 4s </summary>
    public static IEnumerator DH_HM_MS_TimerRoutine(TMP_Text timerText, DateTime timeEnd, Action onTimerEnd = null, string extraFormat = null)
    {
        TimeSpan timeRemain = (timeEnd - DateTime.Now);

        var time = string.Empty;
        while (timeRemain.TotalSeconds > 0)
        {
            var d = (int)timeRemain.TotalDays;
            var h = timeRemain.Hours;
            var m = timeRemain.Minutes;
            var s = timeRemain.Seconds;

            if (d > 0)
            {
                time = $"{d}d {h}h";
            }
            else if (h > 0)
            {
                time = $"{h}h {m}m";
            }
            else
            {
                time = $"{m}m {s}s";
            }

            timerText.text = (extraFormat == null ? time : extraFormat.Format(time));

            yield return YieldCollection.WaitForSeconds(1);
            timeRemain = timeEnd - DateTime.Now;
        }

        onTimerEnd?.Invoke();
    }

    /// <summary> 23:34:45 </summary>
    public static IEnumerator TimerStartRoutine(TMP_Text timerText, Func<bool> stopCondition = null, string format = "hh':'mm':'ss")
    {
        var startDate = DateTimeOffset.UtcNow;
        var timeElapsed = TimeSpan.FromSeconds(0);

        var time = timeElapsed.ToString(format);
        timerText.text = time;

        while (stopCondition == null || !stopCondition())
        {
            yield return YieldCollection.WaitForSeconds(0.1f);
            timeElapsed = DateTimeOffset.UtcNow - startDate;
            time = timeElapsed.ToString(format);
            timerText.text = time;
        }
    }

    /// <summary> 23:34:45 </summary>
    public static IEnumerator TimerEndRoutine(TMP_Text timerText, DateTime timeEnd, Action onTimerEnd = null, string format = "hh':'mm':'ss")
    {
        TimeSpan timeRemain = (timeEnd - DateTime.Now);

        var time = timeRemain.ToString(format);
        timerText.text = time;

        while (timeRemain.TotalSeconds > 0)
        {
            yield return YieldCollection.WaitForSeconds(0.1f);
            timeRemain = timeEnd - DateTime.Now;
            time = timeRemain.ToString(format);
            timerText.text = time;
        }

        onTimerEnd?.Invoke();
    }

    public static IEnumerator WaitUntilStableFPS(float threshold = 0.75f)
    {
        yield return null;
        while ((1 / Time.deltaTime) < (Application.targetFrameRate * threshold))
        {
            yield return null;
        }
    }

    public static IEnumerator WaitUntil(Func<bool> condition)
    {
        while (true)
        {
            if (condition == null || condition())
            {
                yield break;
            }
            yield return null;
        }
    }
    public static IEnumerator WaitClick(Action onClick, Func<bool> condition = null)
    {
        yield return null;
        while (true)
        {
            if ((condition == null || condition()) && Input.GetMouseButtonDown(0))
            {
                onClick?.Invoke();
                yield break;
            }
            yield return null;
        }
    }

    public static IEnumerator RepeatInterval(float interval, Action action)
    {
        while (true)
        {
            yield return YieldCollection.WaitForSeconds(interval);
            action?.Invoke();
        }
    }


    public static IEnumerator AutoSelectInputFieldRoutine(TMP_InputField inputField, float delay = 0f)
    {
        yield return YieldCollection.WaitForSeconds(delay);
        while (!UIManager.IsInteractable)
        {
            yield return null;
        }
        yield return null;
        inputField.Select();
    }
}
