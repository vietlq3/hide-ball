using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(VectorPickerAttribute))]
public class VectorPickerDrawer : PropertyDrawer
{
    private bool _trackMouse;
    private SerializedProperty _property;
    private MonoBehaviour _script;

    ///<summary>For reverting if tracking canceled</summary>
    private Vector2 _originalPosition;

    private Vector2 _finalPos;
    private bool _setup;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.Vector2)
        {
            EditorGUI.HelpBox(position, "This Attribute requires Vector2", MessageType.Error);
            return;
        }

        _script = (MonoBehaviour)property.serializedObject.targetObject;
        if (_script == null || _script.gameObject == null)
        {
            EditorGUI.HelpBox(position, "This script must be attached to a gameObject", MessageType.Error);
            return;
        }


        if (!_setup)
        {
            _property = property;
            _setup = true;
        }

        GUI.enabled = !_trackMouse;
        EditorGUI.PropertyField(position, property, label);
        GUI.enabled = true;

        var e = Event.current;

        if (!_trackMouse)
        {
            var buttonRect = new Rect(position);
            buttonRect.x = position.width - 25;
            buttonRect.width = 35;

            // if button wasn't pressed do nothing
            if (!GUI.Button(buttonRect, "Pick")) return;

            // store current value in case of revert
            _originalPosition = _property.vector2Value;

            // enable tracking
            _property = property;
            _trackMouse = true;

            // Lock the inspector so we cannot lose focus
            ActiveEditorTracker.sharedTracker.isLocked = true;

            // Prevent event propagation
            e.Use();

            //Debug.Log("Vector Picker started");
            return;
        }

        // <<< This section is only reached if we are in tracking mode >>>

        // Overwrite the onSceneGUIDelegate with a callback for the SceneView
        SceneView.duringSceneGui += UpdateSceneView;

        // Set to world position
        _property.vector2Value = _finalPos;

        // Track position until either Mouse button 0 (to confirm) or Escape (to cancel) is clicked
        var mouseUpDown = (e.type == UnityEngine.EventType.MouseUp || e.type == UnityEngine.EventType.MouseDown) && e.button == 0;
        if (mouseUpDown)
        {
            // End the tracking, don't revert
            _property.vector2Value = OnTrackingEnds(false, e);
        }
        else if (e.type == UnityEngine.EventType.KeyUp && _trackMouse && e.keyCode == KeyCode.Escape)
        {
            // Cancel tracking via Escape => revert value
            _property.vector2Value = OnTrackingEnds(true, e);
        }

        _property.serializedObject.ApplyModifiedProperties();

        //This fixes "randomly stops updating for no reason".
        EditorUtility.SetDirty(_property.serializedObject.targetObject);
    }


    private void UpdateSceneView(SceneView sceneView)
    {
        var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        _finalPos = ray.origin;

        VectorPickerAttribute vectorPicker = attribute as VectorPickerAttribute;
        if (_script != null && vectorPicker.toLocalPos) _finalPos = _script.transform.parent.InverseTransformPoint(_finalPos);

        // get current event
        var e = Event.current;

        // Only check while tracking
        if (_trackMouse)
        {
            if ((e.type == UnityEngine.EventType.MouseDown || e.type == UnityEngine.EventType.MouseUp) && e.button == 0)
            {
                OnTrackingEnds(false, e);
            }
            else
            {
                // Prevent losing focus
                Selection.activeGameObject = _script.gameObject;
            }
        }
        else
        {
            if (e.type == UnityEngine.EventType.Layout || e.type == UnityEngine.EventType.Repaint) return;

            // Prevent Propagation
            Event.current.Use();
            Event.current = null;

            // Unlock Inspector
            ActiveEditorTracker.sharedTracker.isLocked = false;

            // Prevent losing focus
            Selection.activeGameObject = _script.gameObject;

            // Remove SceneView callback
            SceneView.duringSceneGui -= UpdateSceneView;

        }
    }

    /// <summary>
    /// Called when ending Tracking
    /// </summary>
    /// <param name="revert">flag whether to revert to previous value or not</param>
    /// <param name="e">event that caused the ending</param>
    /// <returns>Returns the vector value of the property that we are modifying.</returns>
    private Vector2 OnTrackingEnds(bool revert, Event e)
    {
        e.Use();
        Event.current = null;
        //Debug.Log("Vector Picker finished");

        if (revert)
        {
            // restore previous value
            _property.vector2Value = _originalPosition;
            //Debug.Log("Reverted");
        }

        // disable tracking
        _trackMouse = false;

        // Apply changes
        _property.serializedObject.ApplyModifiedProperties();

        return _property.vector2Value;
    }
}