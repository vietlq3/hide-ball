using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attribute for setting a vector by clicking on the screen in editor mode.
/// </summary>

[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class VectorPickerAttribute : PropertyAttribute
{
    public readonly bool toLocalPos;

    /// <summary> Pick a vector using mouse position </summary>
    /// <param name="toLocalPos"> Returns local pos instead of world pos </param>
    public VectorPickerAttribute(bool toLocalPos = true)
    {
        this.toLocalPos = toLocalPos;
    }
}