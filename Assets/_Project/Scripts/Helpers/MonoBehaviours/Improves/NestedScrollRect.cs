using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary> Pass the scroll event of this ScrollRect to the parent's ScrollRect </summary>
public class NestedScrollRect : ScrollRect
{
    private ScrollRect _parentScroll;
    private bool _draggingParent = false;

    public Action<PointerEventData> OnBeginDragParentCallback;
    public Action<PointerEventData> OnDragParentCallback;
    public Action<PointerEventData> OnEndDragParentCallback;

    public void SetParentScroll(ScrollRect parentScroll)
    {
        _parentScroll = parentScroll;
    }

    private bool TrySetParentScroll()
    {
        if (_parentScroll != null) { return true; }

        _parentScroll = transform.parent.GetComponentInParent<ScrollRect>();

        return _parentScroll != null;
    }

    bool IsPotentialParentDrag(Vector2 inputDelta)
    {
        if (!TrySetParentScroll()) { return false; }

        if (_parentScroll.horizontal && !_parentScroll.vertical)
        {
            if (horizontal) { return false; }
            return Mathf.Abs(inputDelta.x) > 1.5f * Mathf.Abs(inputDelta.y);
        }
        if (!_parentScroll.horizontal && _parentScroll.vertical)
        {
            return Mathf.Abs(inputDelta.x) < Mathf.Abs(inputDelta.y);
        }
        return true;
    }

    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        base.OnInitializePotentialDrag(eventData);

        if (!TrySetParentScroll()) { return; }
        _parentScroll.OnInitializePotentialDrag(eventData);
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        if (!IsPotentialParentDrag(eventData.delta))
        {
            base.OnBeginDrag(eventData);
            return;
        }

        _parentScroll.OnBeginDrag(eventData);
        OnBeginDragParentCallback?.Invoke(eventData);
        _draggingParent = true;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (!_draggingParent)
        {
            base.OnDrag(eventData);
            return;
        }

        OnDragParentCallback?.Invoke(eventData);
        _parentScroll.OnDrag(eventData);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        if (!_draggingParent) { return; }

        _draggingParent = false;
        _parentScroll.OnEndDrag(eventData);
        OnEndDragParentCallback?.Invoke(eventData);
    }
}