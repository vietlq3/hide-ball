using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ImprovedDefaultComponents
{
    public class ImprovedScrollRect : ScrollRect
    {
        public Action<PointerEventData> OnBeginDragCallback;
        public Action<PointerEventData> OnDragCallback;
        public Action<PointerEventData> OnEndDragCallback;

        private SwipeInfo _swipeInfo = new SwipeInfo();
        public SwipeInfo LastSwipeInfo => _swipeInfo;

        public override void OnBeginDrag(PointerEventData eventData)
        {
            base.OnBeginDrag(eventData);
            OnBeginDragCallback?.Invoke(eventData);

            _swipeInfo.dragDelta = Vector2.zero;
            _swipeInfo.dragDuration = Time.time;
        }

        public override void OnDrag(PointerEventData eventData)
        {
            base.OnDrag(eventData);
            _swipeInfo.dragDelta += eventData.delta;

            OnDragCallback?.Invoke(eventData);
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            base.OnEndDrag(eventData);
            _swipeInfo.dragDuration = Time.time - _swipeInfo.dragDuration;

            OnEndDragCallback?.Invoke(eventData);
        }


        public struct SwipeInfo
        {
            public float dragDuration;

            /// <summary>  Less than 0 means dragged left </summary>
            public Vector2 dragDelta;
        }
    }
}