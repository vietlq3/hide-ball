﻿using UnityEngine.Events;

namespace UnityEngine.EventSystems
{
	[DisallowMultipleComponent]
	public class PinchInputDetector : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
	{
		/* Calling Order: OnBeginDrag -> OnDrag -> OnEndDrag */

		private IPinchStartHandler[] _pinchStartHandlers;
		private IPinchEndHandler[] _pinchEndHandlers;
		private IPinchZoomHandler[] _pinchZoomHandlers;

		private int _touchCount = 0;
		private bool _isPinching = false;

		private PointerEventData _firstPointer;
		private PointerEventData _secondPointer;
		private float _previousDistance = 0f;
		private float _delta = 0f;

		[SerializeField] protected UnityEvent _onPinchStart;
		[SerializeField] protected UnityEvent _onPinchEnd;

		public UnityEvent onPinchStart { get => _onPinchStart; set => _onPinchStart = value; }
		public UnityEvent onPinchEnd { get => _onPinchEnd; set => _onPinchEnd = value; }

		protected virtual void Awake()
		{
			_pinchStartHandlers = GetComponents<IPinchStartHandler>();
			_pinchEndHandlers = GetComponents<IPinchEndHandler>();
			_pinchZoomHandlers = GetComponents<IPinchZoomHandler>();
		}

		// Similar behaviour as OnPointerDown but we are using OnBeginDrag to avoid complication
		protected virtual void RegisterPointer(PointerEventData eventData)
		{
			_touchCount++;
			if (_firstPointer == null)
			{
				// This is first touch
				_firstPointer = eventData;
			}
			else if (_secondPointer == null)
			{
				// This is second touch
				_secondPointer = eventData;
				CalculateDistanceDelta();
				if (_touchCount >= 2)
				{
					_isPinching = true;
					FireOnPinchStart(new PinchEventData(_secondPointer, _firstPointer));
				}
			}
			// else: ignore any third touches
		}

		// Similar behaviour as OnPointerUp but we are using OnEndDrag to avoid complication
		protected virtual void UnregisterPointer(PointerEventData eventData)
		{
			_touchCount--;
			if (_touchCount < 0) Debug.LogError("PinchInputDetector - Touch Count Mismatch");
			if (IsEqualPointer(_firstPointer, eventData))
			{
				// first touch removed
				if (_isPinching)
				{
					_isPinching = false;
					FireOnPinchEnd(new PinchEventData(_firstPointer, _secondPointer));
				}

				// second touch becomes first touch
				if (_secondPointer != null)
				{
					_firstPointer = _secondPointer;
					_secondPointer = null;
				}
				else
				{
					_firstPointer = null;
				}
			}
			else if (IsEqualPointer(_secondPointer, eventData))
			{
				// second touch removed
				if (_isPinching)
				{
					_isPinching = false;
					FireOnPinchEnd(new PinchEventData(_secondPointer, _firstPointer));
				}
				_secondPointer = null;
			}
		}

		public virtual void OnBeginDrag(PointerEventData eventData)
		{
			RegisterPointer(eventData);
			if (_touchCount == 1) return; // default behaviour
			if (_isPinching) eventData.Use();
			else if (!IsEqualPointer(_firstPointer, eventData) && !IsEqualPointer(_secondPointer, eventData))
			{
				// ignore third touch - consume the event
				eventData.Use();
			}
		}

		public virtual void OnEndDrag(PointerEventData eventData)
		{
			UnregisterPointer(eventData);
			if (_touchCount == 0) return; // default behaviour
										  // OnPointerUp is called before, therefore firstPointer and secondPointer may both be null
			if (_isPinching) eventData.Use();
			else if (!IsEqualPointer(_firstPointer, eventData) && !IsEqualPointer(_secondPointer, eventData))
			{
				// ignore third touch - consume the event
				eventData.Use();
			}
		}

		public virtual void OnDrag(PointerEventData eventData)
		{
			if (_touchCount == 0) return; // not our business - do not consume the event
			if (IsEqualPointer(_firstPointer, eventData))
			{
				// first touch dragging
				_firstPointer = eventData;
				if (_secondPointer != null) CalculateDistanceDelta();
				if (_isPinching)
				{
					eventData.Use();
					FireOnPinchZoom(new PinchEventData(_firstPointer, _secondPointer, _delta));
				}
			}
			else if (IsEqualPointer(_secondPointer, eventData))
			{
				// second touch dragging
				_secondPointer = eventData;
				if (_firstPointer != null) CalculateDistanceDelta();
				if (_isPinching)
				{
					eventData.Use();
					FireOnPinchZoom(new PinchEventData(_secondPointer, _firstPointer, _delta));
				}
			}
			else
			{
				// ignore third touch dragging - consume the event
				eventData.Use();
			}
		}

		private bool IsEqualPointer(PointerEventData a, PointerEventData b)
		{
			if (a == null) return false;
			if (b == null) return false;
			return a.pointerId == b.pointerId;
		}

		protected virtual void CalculateDistanceDelta()
		{
			float newDistance = Vector2.Distance(_firstPointer.position, _secondPointer.position);
			var isSameDirection = Vector2.Dot(_firstPointer.delta, _secondPointer.delta) > 0;
			_delta = isSameDirection ? 0 : newDistance - _previousDistance;
			_previousDistance = newDistance;
		}

		protected virtual void FireOnPinchStart(PinchEventData data)
		{
			if (_onPinchStart != null)
			{
				_onPinchStart.Invoke();
			}
			if (_pinchStartHandlers == null) return;
			for (int i = 0; i < _pinchStartHandlers.Length; i++)
			{
				_pinchStartHandlers[i].OnPinchStart(data);
			}
		}

		protected virtual void FireOnPinchEnd(PinchEventData data)
		{
			if (_onPinchEnd != null)
			{
				_onPinchEnd.Invoke();
			}
			if (_pinchEndHandlers == null) return;
			for (int i = 0; i < _pinchEndHandlers.Length; i++)
			{
				_pinchEndHandlers[i].OnPinchEnd(data);
			}
		}

		protected virtual void FireOnPinchZoom(PinchEventData data)
		{
			if (_pinchZoomHandlers == null) return;
			for (int i = 0; i < _pinchZoomHandlers.Length; i++)
			{
				_pinchZoomHandlers[i].OnPinchZoom(data);
			}
		}
	}
}