using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteStretchFillScreen : MonoBehaviour
{
    [SerializeField] private bool _keepAspectRatio;
    [SerializeField] private Camera _camera;

    void Start()
    {
        if (_camera == null) { _camera = Camera.main; }

        transform.localScale = Vector3.one;

        var topRightCorner = _camera.ViewportToWorldPoint(Vector2.one);
        var worldSpaceWidth = topRightCorner.x * 2;
        var worldSpaceHeight = topRightCorner.y * 2;

        var spriteSize = GetComponent<SpriteRenderer>().bounds.size;

        var scaleFactorX = worldSpaceWidth / spriteSize.x;
        var scaleFactorY = worldSpaceHeight / spriteSize.y;

        if (_keepAspectRatio)
        {
            if (scaleFactorX > scaleFactorY)
            {
                scaleFactorY = scaleFactorX;
            }
            else
            {
                scaleFactorX = scaleFactorY;
            }
        }

        transform.localScale = new Vector3(scaleFactorX, scaleFactorY, 1);
    }
}