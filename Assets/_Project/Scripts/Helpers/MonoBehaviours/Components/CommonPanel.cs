using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall
{
    public enum Direction { LeftToRight, RightToLeft }

    public abstract class CommonPanel<T> : MonoBehaviour where T : CommonPanel<T>
    {
        private UIAnim _cacheUiAnim;
        public UIAnim Anim => _cacheUiAnim ??= this.ForceGetComponent<UIAnim>();
        public CanvasGroup canvasGroup => Anim.canvasGroup;
        public RectTransform rectTransform => transform as RectTransform;

        private int _index = -1;

        public Action OnClickNext, OnClickBack;
        public T PrevPanel { get; private set; }
        public T NextPanel { get; private set; }
        public bool CanBack { get; set; } = true;

        public int Index => _index < 0 ? transform.GetSiblingIndex() : _index;

        /// <summary> -1 will default to SiblingIndex </summary>
        public void SetIndex(int index)
        {
            _index = index;
        }

        public virtual void Init() { }
        public virtual void OnShow() { }
        public virtual void OnHide() { }


        public void SetNavigation(T prevPanel, T nextPanel)
        {
            PrevPanel = prevPanel;
            NextPanel = nextPanel;
        }

        public virtual void TransitionIn(Direction direction, float duration = UIManager.DefaultTransitionDuration)
        {
            gameObject.SetActive(true);

            var baseOffset = rectTransform.rect.width;
            var xOffset = baseOffset * (direction == Direction.LeftToRight ? -1 : 1);
            rectTransform.anchoredPosition = new Vector3(xOffset, 0);

            rectTransform.DOAnchorPosX(0, duration);
            Anim.FadeIn(duration);
        }

        public virtual void TransitionOut(Direction direction, float duration = UIManager.DefaultTransitionDuration)
        {
            gameObject.SetActive(true);

            var baseOffset = rectTransform.rect.width;
            var xOffset = baseOffset * (direction == Direction.LeftToRight ? 1 : -1);
            rectTransform.anchoredPosition = Vector3.zero;

            rectTransform.DOAnchorPosX(xOffset, duration);
            Anim.FadeOut(duration);
        }

#if UNITY_EDITOR
        [Button]
        private void Select()
        {
            foreach (var panel in transform.parent.GetComponentsInChildren<CommonPanel<T>>(true))
            {
                panel.gameObject.SetActive(panel == this);
            }
        }
#endif
    }
}