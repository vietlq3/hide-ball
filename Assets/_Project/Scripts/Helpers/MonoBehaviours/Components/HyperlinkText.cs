using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall;
using Seekers.HideBall.UI;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TMP_Text))]
public class HyperlinkText : MonoBehaviour, IPointerClickHandler
{
    [Serializable]
    public class LinkMap
    {
        public string Id, Url;
        public UnityEvent OnClick = new();
    }

    [SerializeField] private LinkMap[] _links;

    private TMP_Text _text;

    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
        _text.raycastTarget = true;
    }

    /// <summary> ID and Link </summary>
    public void SetLinks(params LinkMap[] links)
    {
        _links = links;
    }

    public LinkMap GetLink(string id)
    {
        var link = _links.Find(link => link.Id == id);
        return link;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        var linkIndex = TMP_TextUtilities.FindIntersectingLink(_text, Input.mousePosition, UIManager.UICamera);
        if (linkIndex < 0 || linkIndex > _links.Length - 1) { return; }

        var linkInfo = _text.textInfo.linkInfo[linkIndex];

        var link = _links.Find(link => link.Id == linkInfo.GetLinkID());
        if (link == null) { return; }

        if (!link.Url.IsNullOrEmpty())
        {
            Application.OpenURL(link.Url);
        }

        link.OnClick?.Invoke();
    }
}
