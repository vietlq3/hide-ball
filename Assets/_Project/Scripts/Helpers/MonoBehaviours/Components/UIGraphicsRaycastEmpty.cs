﻿using UnityEngine;
using UnityEngine.UI;

namespace Athena.Common.UI
{
    [RequireComponent(typeof(CanvasRenderer))]
    public class UIGraphicsRaycastEmpty : Graphic
    {
        protected override void UpdateGeometry()
        {

        }
    }
}
