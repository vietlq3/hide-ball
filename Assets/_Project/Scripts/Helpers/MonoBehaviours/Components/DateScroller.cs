using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DanielLochner.Assets.SimpleScrollSnap;
using DG.DemiLib;
using Seekers.HideBall;
using Seekers.HideBall.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DateScroller : MonoBehaviour
{
    [field: SerializeField] public Button DoneBtn { get; private set; }

    [SerializeField] private SimpleScrollSnap _monthScrollSnap, _dayScrollSnap, _yearScrollSnap;

    [Tooltip("MM/dd/yyyy")]
    [SerializeField] private string _defaultDate = "12/31/2005";
    [SerializeField] private IntRange _minMaxYear = new IntRange(1940, 2005);

    private UIAnim _cacheAnim;
    public UIAnim Anim => _cacheAnim ??= this.ForceGetComponent<UIAnim>();


    private void Awake()
    {
        var date = _defaultDate.ToDate();

        SetupMonthScroll(date.Month);
        SetupDayScroll(date);
        SetupYearScroll(date.Year);
    }

    private void SetupMonthScroll(int defaultMonth)
    {
        if (_monthScrollSnap == null) { return; }

        var firstMonthName = CultureInfo.InvariantCulture.DateTimeFormat.GetMonthName(1);
        _monthScrollSnap.GetSample<TextMeshProUGUI>().text = firstMonthName;
        for (int month = 2; month <= 12; month++)
        {
            var added = _monthScrollSnap.AddToBack<TextMeshProUGUI>(false);
            added.text = CultureInfo.InvariantCulture.DateTimeFormat.GetMonthName(month);
        }
        _monthScrollSnap.StartingPanel = defaultMonth - 1;
        _monthScrollSnap.OnPanelSelected.AddListener((_) => RefreshDaysInMonth());
    }
    private void SetupDayScroll(DateTime defaultDate)
    {
        if (_dayScrollSnap == null) { return; }

        _dayScrollSnap.GetSample<TextMeshProUGUI>().text = "1";
        var daysInMonth = DateTime.DaysInMonth(defaultDate.Year, defaultDate.Month);
        for (int day = 2; day <= daysInMonth; day++)
        {
            var added = _dayScrollSnap.AddToBack<TextMeshProUGUI>(false);
            added.text = $"{day}";
        }
        _dayScrollSnap.StartingPanel = defaultDate.Day - 1;
    }
    private void SetupYearScroll(int defaultYear)
    {
        if (_yearScrollSnap == null) { return; }

        var startYear = _minMaxYear.min;
        var endYear = _minMaxYear.max;
        _yearScrollSnap.GetSample<TextMeshProUGUI>().text = $"{startYear}";
        for (int year = startYear + 1; year <= endYear; year++)
        {
            var added = _yearScrollSnap.AddToBack<TextMeshProUGUI>(false);
            added.text = $"{year}";
        }
        _yearScrollSnap.StartingPanel = defaultYear - startYear;
        _yearScrollSnap.OnPanelSelected.AddListener((_) => RefreshDaysInMonth());
    }

    private void RefreshDaysInMonth()
    {
        var month = GetText(_monthScrollSnap);
        var year = GetText(_yearScrollSnap);
        var dateString = $"{month} {year}";
        var date = dateString.ToDate();
        var daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);

        if (_dayScrollSnap.NumberOfPanels == daysInMonth) { return; }

        var dayObjCount = _dayScrollSnap.NumberOfPanels;
        var diff = Mathf.Abs(dayObjCount - daysInMonth);

        for (int i = 0; i < diff; i++)
        {
            if (dayObjCount < daysInMonth)
            {
                var dayObj = _dayScrollSnap.AddToBack<TextMeshProUGUI>();
                var day = dayObjCount + i + 1;
                dayObj.text = $"{day}";
            }
            else
            {
                _dayScrollSnap.RemoveFromBack();
            }
        }
    }

    public DateTime Date
    {
        get
        {
            var month = GetText(_monthScrollSnap);
            var day = GetText(_dayScrollSnap);
            var year = GetText(_yearScrollSnap);
            var dateString = $"{month ?? "January"} {day ?? "1"} {year ?? "1"}";
            return dateString.ToDate();
        }
    }

    private string GetText(SimpleScrollSnap scrollSnap)
    {
        if (scrollSnap == null) { return null; }
        var textComponent = scrollSnap.Panels[scrollSnap.CenteredPanel].GetComponentInChildren<TextMeshProUGUI>();
        return textComponent.text;
    }

    public static DateScroller Show(Vector3 pos, string defaultDate = null, int? minYear = null, int? maxYear = null)
    {
        var resource = Resources.Load<DateScroller>("DateScroller");
        if (minYear != null && maxYear != null)
        {
            resource._minMaxYear = new IntRange(minYear.Value, maxYear.Value);
        }
        if (defaultDate != null)
        {
            resource._defaultDate = defaultDate;
        }

        var instance = Instantiate(resource, UIManager.UILayer);
        instance.transform.GetChild(1).position = pos;
        return instance;
    }
}