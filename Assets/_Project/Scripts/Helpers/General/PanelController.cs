using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall
{
    [Serializable]
    public class PanelController<T> where T : CommonPanel<T>
    {
        [SerializeField] private T[] _panels;

        private T _currentPanel;

        public void ForEach(Action<T> action)
        {
            foreach (var panel in _panels)
            {
                action?.Invoke(panel);
            }
        }

        public void GoTo<U>(bool animate = true) where U : T
        {
            var panel = Get<U>();
            GoTo(panel, animate);
        }

        protected virtual void AnimatePanelTransition(T panel)
        {
            UIManager.DisableInteractionFor(0.3f);
            var movementType = _currentPanel.Index < panel.Index ? Direction.RightToLeft : Direction.LeftToRight;
            _currentPanel.TransitionOut(movementType);
            panel.TransitionIn(movementType);
        }

        public void GoTo(T panel, bool animate = true)
        {
            if (animate && _currentPanel != null)
            {
                AnimatePanelTransition(panel);
            }

            panel.gameObject.SetActive(true);
            panel.OnShow();

            _currentPanel?.OnHide();
            _currentPanel = panel;
        }

        public U Get<U>() where U : T
        {
            var panel = _panels.Find(match => match is U);
            return panel as U;
        }
    }
}
