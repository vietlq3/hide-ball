using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class NewtonsoftSerializer
{
    public static T FromJson<T>(string json)
    {
        return JsonConvert.DeserializeObject<T>(json);
    }

    public static object FromJson(string json)
    {
        return JsonConvert.DeserializeObject(json);
    }

    public static string ToJson(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }
}
