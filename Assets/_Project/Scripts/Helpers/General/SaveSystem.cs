using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SaveSystem
{
    public enum SerializeType { JsonUtility, NewtonsoftJson }

    public class Encrypted
    {
        public static void Save(string filePath, string data)
        {
            data = Encryption.XOR(data);
            SaveSystem.Save(filePath, data);
        }

        public static string Load(string filePath)
        {
            var dataString = SaveSystem.Load(filePath);
            return Encryption.XOR(dataString);
        }

        public static T Load<T>(string filePath, SerializeType serializeType = SerializeType.JsonUtility)
        {
            var dataString = Load(filePath);
            if (dataString.IsNullOrEmpty()) { return default; }

            T result = default;
            try
            {
                switch (serializeType)
                {
                    case SerializeType.JsonUtility:
                        result = JsonUtility.FromJson<T>(dataString);
                        break;

                    case SerializeType.NewtonsoftJson:
                        result = NewtonsoftSerializer.FromJson<T>(dataString);
                        break;
                }
            }
            catch (Exception)
            {
                Debug.LogError("Invalid JSON format: " + dataString);
            }

            return result;
        }
    }

#if UNITY_EDITOR
    // [UnityEditor.MenuItem("SaveSystem/Test")]
    private static void Test()
    {
        var dataString = "";

        var test1path = GetObscuredPath("Test/Test1");
        var test2path = GetObscuredPath("Test/Test2");

        Save(test1path, Encryption.XOR(dataString));
        Debug.Log($"Test1 path: {test1path}");

        var loadedData = Load(test1path);
        Save(test2path, Encryption.XOR(loadedData));
        Debug.Log($"Test2 path: {test2path}");
    }

    [MenuItem("SaveSystem/Open Save Path")]
    private static void OpenSavePath()
    {
        EditorUtility.RevealInFinder(Application.persistentDataPath);
    }

    [MenuItem("SaveSystem/Decipher")]
    private static void Decipher()
    {
        var path = EditorUtility.OpenFilePanel("Select text file to decipher", Application.persistentDataPath, "");
        if (path.Length != 0)
        {
            var dataString = Encrypted.Load(path);
            var fileName = Path.GetFileName(path).FromHex();
            Debug.LogWarning($"File name: {fileName}, data:\n{dataString}");
        }
    }
#endif

    public static bool HasSave(string filePath)
    {
        return File.Exists(filePath);
    }

    public static void Save(string filePath, string data)
    {
        try
        {
            File.WriteAllText(filePath, data);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static string Load(string filePath)
    {
        var text = string.Empty;
        try
        {
            text = File.ReadAllText(filePath);
        }
        catch (Exception)
        {
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            try { fileName = fileName.FromHex(); }
            catch (Exception) { }
            Debug.LogWarning($"No save found: {fileName}");
        }

        return text;
    }
    public static T Load<T>(string filePath, SerializeType serializeType = SerializeType.JsonUtility)
    {
        var dataString = Load(filePath);
        if (dataString.IsNullOrEmpty()) { return default; }

        T result = default;
        try
        {
            switch (serializeType)
            {
                case SerializeType.JsonUtility:
                    result = JsonUtility.FromJson<T>(dataString);
                    break;

                case SerializeType.NewtonsoftJson:
                    result = NewtonsoftSerializer.FromJson<T>(dataString);
                    break;
            }
        }
        catch (Exception)
        {
            Debug.LogError("Invalid JSON format: " + dataString);
        }

        return result;
    }

    public static void Delete(string filePath)
    {
        try
        {
            File.Delete(filePath);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static string GetObscuredPath(string filePath)
    {
        var paths = filePath.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < paths.Length; i++)
        {
            paths[i] = paths[i].ToHex();
        }
        filePath = Path.Combine(paths);

        var finalPath = Path.Combine(Application.persistentDataPath, filePath);
        Directory.CreateDirectory(Path.GetDirectoryName(finalPath));

        return finalPath;
    }
}