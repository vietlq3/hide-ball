using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall
{
    public interface IStateMachine
    {
        void ChangeState(IState state);
    }

    public interface IMonoBehaviourStateMachine : IStateMachine
    {
        CoroutineRunner CoroutineRunner { get; }
    }

    public abstract class StateMachine<T> : IStateMachine where T : State
    {
        public event Action<T> OnStateChange;

        private readonly Dictionary<Type, T> _states = new();
        protected IState _currentState;

        public U GetState<U>() where U : T
        {
            if (!_states.TryGetValue(typeof(U), out var state))
            {
                Debug.LogError($"Cannot find state: {typeof(U).Name}");
            }
            return state as U;
        }

        protected void SetupStates(params T[] states)
        {
            _states.Clear();
            foreach (var state in states)
            {
                AddState(state);
            };
        }

        protected void AddState(T state)
        {
            if (!_states.TryAdd(state.GetType(), state))
            {
                Debug.LogError($"State {state.GetType().Name} is already added!");
                return;
            }

            (state as IState).Init();
        }

        protected virtual void StateUpdate()
        {
            if (_currentState == null) { return; }

            _currentState.OnUpdate();
        }

        public void ChangeState<U>() where U : T
        {
            ChangeState(GetState<U>());
        }

        public virtual void ChangeState(IState state)
        {
            _currentState?.OnExit(state);

            if (state == null)
            {
                Debug.LogError($"Trying to change to null state: {state.GetType().Name}, current state: {_currentState?.GetType().Name}");
                return;
            }

            var lastState = _currentState;
            _currentState = state;

            _currentState.OnEnter(lastState);

            OnStateChange?.Invoke(state as T);
        }
    }

    public abstract class MonoBehaviourStateMachine<T> : StateMachine<T>, IMonoBehaviourStateMachine where T : State
    {
        public CoroutineRunner CoroutineRunner { get; private set; }
        public MonoBehaviourStateMachine(MonoBehaviour monoBehaviour)
        {
            CoroutineRunner = new CoroutineRunner(monoBehaviour);
        }
    }
}