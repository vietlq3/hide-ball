using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall
{
    public interface IState
    {
        void Init();
        void OnEnter(IState lastState);
        void OnUpdate();
        void OnExit(IState nextState);
    }

    public abstract class State : IState
    {
        protected readonly IStateMachine _stateMachine;

        private State() { }
        public State(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        protected void ChangeState(IState state)
        {
            _stateMachine.ChangeState(state);
        }

        /// <summary> Should put constructor logic in Init because stateMachine might not be ready </summary>
        protected virtual void Init() { }
        protected virtual void OnEnter(IState lastState) { }
        protected virtual void OnUpdate() { }
        protected virtual void OnExit(IState nextState) { }

        void IState.Init() => Init();
        void IState.OnEnter(IState lastState) => OnEnter(lastState);
        void IState.OnUpdate() => OnUpdate();
        void IState.OnExit(IState nextState) => OnExit(nextState);
    }

    public abstract class MonoBehaviourState : State
    {
        private CoroutineRunner _coroutineRunner;

        public MonoBehaviourState(IMonoBehaviourStateMachine stateMachine) : base(stateMachine)
        {
            _coroutineRunner = stateMachine.CoroutineRunner;
        }

        protected void StartCoroutine(IEnumerator routine) => _coroutineRunner.StartCoroutine(routine);
        protected void StopCoroutine(Coroutine coroutine) => _coroutineRunner.StopCoroutine(coroutine);
        protected void StopAllCoroutines() => _coroutineRunner.StopAllCoroutines();
    }

}