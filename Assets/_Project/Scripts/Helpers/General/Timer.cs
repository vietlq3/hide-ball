using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{


}

public class CoroutineTimer
{
    private MonoBehaviour _runner;
    private Coroutine _timerRoutine;
    private float _tickInterval;
    private TimeSpan _initialTime;
    private TimeSpan _countdownTime;

    public TimeSpan TimeLeft => _countdownTime;
    public Action OnTimerEnd;
    public Action OnTimerTick;

    public CoroutineTimer(MonoBehaviour runner)
    {
        _runner = runner;
    }

    public void Start(TimeSpan countdownTime, float tickInterval = 0)
    {
        Stop();

        _initialTime = countdownTime;
        _countdownTime = countdownTime;
        _tickInterval = tickInterval;

        _timerRoutine = _runner.StartCoroutine(TimerRoutine());


        IEnumerator TimerRoutine()
        {
            while (_countdownTime.TotalSeconds > 0)
            {
                yield return YieldCollection.WaitForSeconds(tickInterval);
                _countdownTime -= TimeSpan.FromSeconds(Time.deltaTime);
                OnTimerTick?.Invoke();
            }

            OnTimerEnd?.Invoke();
        }
    }

    public void Stop()
    {
        if (_timerRoutine != null)
        {
            _runner.StopCoroutine(_timerRoutine);
        }
    }

    public void Reset()
    {
        Start(_initialTime, _tickInterval);
    }
}