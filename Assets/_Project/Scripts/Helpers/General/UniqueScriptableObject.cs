using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class UniqueScriptableObject<T> : ScriptableObject where T : ScriptableObject
{
    protected static T _instance;

#if UNITY_EDITOR
    // Check for duplication when created
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Debug.LogError($"{typeof(T)} must be unique, please do not duplicate the original file", _instance);

            if (AssetDatabase.Contains(this)) // created via ctrl+D
            {
                AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(this));
            }
            else // created via CreateAssetMenu
            {
                DestroyImmediate(this, true);
            }
        }
    }
#endif

    protected virtual void OnEnable()
    {
        if (_instance == null)
        {
            _instance = this as T;
        }
    }

}
