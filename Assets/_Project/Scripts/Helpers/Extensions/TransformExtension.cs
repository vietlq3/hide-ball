using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtension
{
    public static void DestroyChildren(this Transform parent)
    {
        parent.PerformOnChildren(child => child.DestroyGameObject());
    }

    public static int GetChildCount(this Transform parent, bool onlyGetActive)
    {
        var count = 0;
        parent.PerformOnChildren(child =>
        {
            if (onlyGetActive && !child.gameObject.activeInHierarchy) { return; }
            count++;
        });
        return count;
    }

    public static List<Transform> GetChildList(this Transform parent, bool onlyGetActive)
    {
        var childList = new List<Transform>();
        parent.PerformOnChildren(child =>
        {
            if (onlyGetActive && !child.gameObject.activeInHierarchy) { return; }
            childList.Add(child);
        });

        return childList;
    }

    public static void PerformOnChildren(this Transform parent, Action<Transform> action)
    {
        if (action == null) { return; }

        for (int i = parent.childCount - 1; i >= 0; i--)
        {
            action(parent.GetChild(i));
        }
    }

}