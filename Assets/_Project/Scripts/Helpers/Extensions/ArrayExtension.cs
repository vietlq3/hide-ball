using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArrayExtension
{
    public static bool Contains<T>(this T[] array, T element)
    {
        return Array.Exists(array, (match) => match.Equals(element));
    }
    public static bool TrueForAll<T>(this T[] array, Predicate<T> match)
    {
        return Array.TrueForAll(array, match);
    }
    public static bool Exists<T>(this T[] array, Predicate<T> match)
    {
        return Array.Exists(array, match);
    }
    public static T Find<T>(this T[] array, Predicate<T> match)
    {
        return Array.Find(array, match);
    }
    public static T[] FindAll<T>(this T[] array, Predicate<T> match)
    {
        return Array.FindAll(array, match);
    }

}
