using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Seekers.HideBall.Audio;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall.GameFlowStates
{
    public class MainMenuState : GameFlowState
    {
        private MainMenuUI _mainMenuUI;

        public MainMenuState(GameFlowStateMachine stateMachine) : base(stateMachine) { }

        protected override void Init()
        {
        }

        protected override void OnEnter(IState lastState)
        {
            AudioManager.PlayMusic(AudioAsset.Music.christmas);
            UIManager.FadeTransition(FadeSetting.FadeOut());

            _mainMenuUI = UIManager.ShowUI<MainMenuUI>();
            var resumePanel = _mainMenuUI.PanelController.Get<ResumePanel>();
            resumePanel.DescriptionText = "Welcome!\nTap to play";
            resumePanel.OnClickOk = () => ChangeToGameState();
            _mainMenuUI.PanelController.GoTo(resumePanel);
        }

        protected override void OnExit(IState nextState)
        {
            UIManager.Hide(_mainMenuUI, immediate: true);
        }


        private void ChangeToGameState()
        {
            var fadeSetting = FadeSetting.Default;
            fadeSetting.OnFadeInComplete = () =>
            {
                ChangeState<ClassicGameState>();
            };
            UIManager.FadeTransition(fadeSetting);
        }

        public override void Preload()
        {
            UIManager.PreloadUI<MainMenuUI>();
            Preload<ClassicGameState>();
        }



    }
}
