using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall.GameFlowStates
{
    public class InitState : GameFlowState
    {
        public InitState(GameFlowStateMachine stateMachine) : base(stateMachine) { }

        protected override async void OnEnter(IState lastState)
        {
            var minWaitTask = UniTask.WaitForSeconds(1f);

            await UniTask.NextFrame();
            await UniTask.WaitUntil(() => (1 / Time.deltaTime) > (Application.targetFrameRate * 0.75f));

            var fadeDuration = 0.5f;
            UIManager.FadeTransition(FadeSetting.FadeOut(fadeDuration));
            var splashUI = UIManager.ShowUI<SplashUI>();

            await UniTask.WaitForSeconds(fadeDuration);

            // Start Init

            DG.Tweening.DOTween.Init();
            InitDevice();

            Preload<MainMenuState>();

            // End Init

            await minWaitTask;

            splashUI.SetText("Completed!");
            await UniTask.WaitForSeconds(0.2f);

            UIManager.FadeTransition(FadeSetting.FadeIn(fadeDuration));
            await UniTask.WaitForSeconds(fadeDuration);
            ChangeState<MainMenuState>();

            UIManager.Hide(splashUI);
        }


        private void InitDevice()
        {
            Application.targetFrameRate = 60;
            // Input.multiTouchEnabled = false;

            // https://developer.android.com/training/multiscreen/screendensities#dips-pels
            var defaultDragThreshold = UIManager.EventSystem.pixelDragThreshold;
            var acceptableDragThreshold = (int)(defaultDragThreshold * (Screen.dpi / 160f));
            UIManager.EventSystem.pixelDragThreshold = Mathf.Max(defaultDragThreshold, acceptableDragThreshold);
        }
    }
}
