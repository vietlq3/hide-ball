using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall.GameFlowStates
{
    public abstract class GameFlowState : MonoBehaviourState
    {
        protected bool _isPreloaded = false;
        public GameFlowState(GameFlowStateMachine stateMachine) : base(stateMachine) { }

        private GameFlowState GetState<T>() where T : GameFlowState
        {
            return (_stateMachine as GameFlowStateMachine).GetState<T>();
        }

        protected void ChangeState<T>() where T : GameFlowState
        {
            _stateMachine.ChangeState(GetState<T>());
        }

        protected void Preload<T>() where T : GameFlowState
        {
            GetState<T>().Preload();
        }

        public virtual void Preload() { }
    }
}
