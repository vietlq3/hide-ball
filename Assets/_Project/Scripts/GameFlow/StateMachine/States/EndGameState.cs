using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.Audio;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall.GameFlowStates
{
    public class EndGameState : GameFlowState
    {
        private EndGameUI _endGameUI;

        public EndGameState(GameFlowStateMachine stateMachine) : base(stateMachine) { }

        protected override void OnEnter(IState lastState)
        {
            AudioManager.PlayMusic(AudioAsset.Music.end_game);

            _endGameUI = UIManager.ShowUI<EndGameUI>();

            _endGameUI.OnClickResult = () =>
            {
                AudioManager.PlaySound(AudioAsset.Sound.btn_tap);

                var userName = AccountManager.UserProfile.UserName;
                var deviceInfo = $"Device ID: {SystemInfo.deviceUniqueIdentifier}";

            };

            _endGameUI.OnClickRestart = () =>
            {
                AudioManager.PlaySound(AudioAsset.Sound.btn_tap);
                GameFlow.Restart();
            };
        }

        protected override void OnExit(IState nextState)
        {
            UIManager.Hide(_endGameUI, immediate: true);
        }
    }
}
