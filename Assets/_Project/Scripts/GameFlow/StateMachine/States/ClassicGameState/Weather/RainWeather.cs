using DG.Tweening;
using UnityEngine;

namespace Seekers.HideBall.Gameplay
{
    public class RainWeather : Weather
    {
        [SerializeField] private float _initialRate = 150;
        private Tween _emissionTween;

        public Tween AdjustIntensity(float intensity, float duration = 0)
        {
            _emissionTween.Kill();
            var emission = _particleSystem.emission;
            emission.rateOverTime = _initialRate;

            _emissionTween = DOVirtual.Float(emission.rateOverTime.constant, _initialRate * intensity, duration, (t) =>
            {
                emission.rateOverTime = t;
            });

            return _emissionTween;
        }

        public Tween AdjustIntensity(float from, float to, float duration)
        {
            _emissionTween.Kill();

            var emission = _particleSystem.emission;
            emission.rateOverTime = _initialRate;

            _emissionTween = DOVirtual.Float(_initialRate * from, _initialRate * to, duration, (t) =>
            {
                emission.rateOverTime = t;
            });

            return _emissionTween;
        }

        public void StartRain(float startDuration)
        {
            AdjustIntensity(0, 1, startDuration).SetEase(Ease.InQuint);
        }

        public void StopRain(float stopDuration)
        {
            AdjustIntensity(0, stopDuration).SetEase(Ease.OutQuint);
        }
    }
}