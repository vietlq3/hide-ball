using UnityEngine;

namespace Seekers.HideBall.Gameplay
{
    [RequireComponent(typeof(ParticleSystem))]
    public abstract class Weather : MonoBehaviour
    {
        [SerializeField] protected ParticleSystem _particleSystem;

        private void Reset()
        {
            _particleSystem = GetComponent<ParticleSystem>();
        }
    }
}