using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Seekers.HideBall.Gameplay.Classic
{
    public abstract class Level : MonoBehaviour
    {
        protected LoseRecap _loseRecap = new();
        protected WinRecap _winRecap = new();

        protected FriendBall[] _friendBalls;
        protected Block[] _blocks;

        protected IDroppable[] _dropElements;

        public Action<WinRecap> OnWin;
        public Action<LoseRecap> OnLose;

        protected virtual void Awake()
        {
            _friendBalls = GetComponentsInChildren<FriendBall>(true);
            _blocks = GetComponentsInChildren<Block>(true);

            _dropElements = GetComponentsInChildren<IDroppable>(true);

            foreach (var block in _blocks)
            {
                block.OnActivated = () => HandleBlockActivated(block);
            }

            foreach (var ball in _friendBalls)
            {
                ball.OnCollided = (other) => HandleBallCollision(ball, other);
                ball.OnOutOfScreen = () => HandleBallOutOfScreen(ball);
                ball.OnActivated = () => HandleBallActivated(ball);
            }
        }

        protected virtual void HandleBallCollision(FriendBall ball, Collision2D other)
        {
            if (other.gameObject.TryGetComponent<EnemyBall>(out _))
            {
                ball.Kill();
                _loseRecap.deathReason = DeathReason.TouchedEnemy;
                Lose();
            }
        }

        protected virtual void HandleBallOutOfScreen(FriendBall ball)
        {
            ball.Kill();
            _loseRecap.deathReason = DeathReason.OutOfScreen;
            Lose();
        }

        protected virtual void HandleBlockActivated(Block block) { }

        protected virtual void HandleBallActivated(FriendBall ball) { }

        protected virtual void Lose()
        {
            OnLose?.Invoke(_loseRecap);
            OnLose = null;
            OnWin = null;
        }

        protected virtual void Win()
        {
            OnWin?.Invoke(_winRecap);
            OnLose = null;
            OnWin = null;
        }
    }
}
