using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Seekers.HideBall.Gameplay.Classic
{
    public class SnowWeatherLevel : Level
    {
        private Fire[] _fireSources;
        private SnowWeather _weather;
        private Coroutine _checkStartWeatherRoutine;
        private bool _canCheckWeather;

        protected override void Awake()
        {
            base.Awake();
            _fireSources = GetComponentsInChildren<Fire>(true);

            foreach (var fire in _fireSources)
            {
                fire.OnActivated = CheckStartWeather;
            }

            foreach (var ball in _friendBalls)
            {
                ball.OnParticleCollided = (other) => HandleParticleCollision(ball, other);
            }


            _weather = GetComponentInChildren<SnowWeather>();
        }

        protected override void HandleBallActivated(FriendBall ball)
        {
            CheckStartWeather();
        }

        protected override void HandleBlockActivated(Block block)
        {
            CheckStartWeather();
        }

        private void HandleParticleCollision(FriendBall ball, GameObject particleSource)
        {
            if (_weather.gameObject != particleSource) { return; }

            if (!_canCheckWeather) { return; }

            var isNearFire = _fireSources.Exists(fire =>
            {
                return Vector3.Distance(fire.transform.position, ball.transform.position) < fire.HeatRadius;
            });

            if (!isNearFire)
            {
                _loseRecap.deathReason = DeathReason.Cold;
                ball.Kill();
                Lose();
            }

        }

        private void CheckStartWeather()
        {
            var allDropped = _dropElements.TrueForAll(match => match.State == DropState.Dropped);
            if (!allDropped)
            {
                return;
            }

            this.SafeStopCoroutine(_checkStartWeatherRoutine);
            _checkStartWeatherRoutine = StartCoroutine(CheckRoutine());


            IEnumerator CheckRoutine()
            {
                var maxWaitSeconds = 3;
                for (int i = 0; i < maxWaitSeconds; i++)
                {
                    yield return YieldCollection.WaitForSeconds(1);
                    var allStopped = _dropElements.TrueForAll(dropper => !dropper.IsMoving());
                    if (allStopped)
                    {
                        break;
                    }
                }

                StartCoroutine(StartWeatherRoutine());
            }
        }

        private IEnumerator StartWeatherRoutine()
        {
            _canCheckWeather = true;
            var maxWaitSeconds = 4;
            for (int i = 0; i < maxWaitSeconds; i++)
            {
                yield return YieldCollection.WaitForSeconds(1);

                foreach (var ball in _friendBalls)
                {
                    var isNearFire = _fireSources.Exists(fire =>
                    {
                        var distance = Vector3.Distance(fire.transform.position, ball.transform.position);
                        return distance < fire.HeatRadius;
                    });

                    if (!isNearFire)
                    {
                        _loseRecap.deathReason = DeathReason.Cold;
                        ball.Kill();
                        Lose();
                    }
                }
            }
            Win();
            _canCheckWeather = false;
        }

    }
}
