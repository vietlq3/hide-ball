using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Seekers.HideBall.Gameplay.Classic
{
    public class RainWeatherLevel : Level
    {
        private RainWeather _weather;
        private Coroutine _checkStartWeatherRoutine;

        protected override void Awake()
        {
            base.Awake();

            foreach (var ball in _friendBalls)
            {
                ball.OnParticleCollided = (other) => HandleParticleCollision(ball, other);
            }

            _weather = GetComponentInChildren<RainWeather>();
            _weather.StopRain(0);
        }

        protected override void HandleBallActivated(FriendBall ball)
        {
            CheckStartWeather();
        }

        protected override void HandleBlockActivated(Block block)
        {
            CheckStartWeather();
        }

        private void HandleParticleCollision(FriendBall ball, GameObject particleSource)
        {
            if (_weather.gameObject != particleSource) { return; }
            _loseRecap.deathReason = DeathReason.Wet;
            ball.Kill();
            Lose();
        }

        private void CheckStartWeather()
        {
            var allDropped = _dropElements.TrueForAll(match => match.State == DropState.Dropped);
            if (!allDropped)
            {
                return;
            }

            this.SafeStopCoroutine(_checkStartWeatherRoutine);
            _checkStartWeatherRoutine = StartCoroutine(CheckRoutine());


            IEnumerator CheckRoutine()
            {
                var maxWaitSeconds = 3;
                for (int i = 0; i < maxWaitSeconds; i++)
                {
                    yield return YieldCollection.WaitForSeconds(1);
                    var allStopped = _dropElements.TrueForAll(dropper => !dropper.IsMoving());
                    if (allStopped)
                    {
                        break;
                    }
                }

                StartCoroutine(StartWeatherRoutine());
            }
        }

        private IEnumerator StartWeatherRoutine()
        {
            _weather.StartRain(2);

            var maxWaitSeconds = 5;
            for (int i = 0; i < maxWaitSeconds; i++)
            {
                yield return YieldCollection.WaitForSeconds(1);
            }

            _weather.StopRain(3);
            yield return YieldCollection.WaitForSeconds(3);
            Win();
        }

    }
}
