using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Seekers.HideBall.Gameplay.Classic
{
    public class EnemyLevel : Level
    {
        [SerializeField] private EnemyBall _enemyBallPrefab;
        [SerializeField] private int _enemySpawnCount;

        private List<EnemyBall> _enemyBalls = new();

        private Coroutine _checkSpawnEnemiesRoutine;

        protected override void Awake()
        {
            base.Awake();

            for (int i = 0; i < _enemySpawnCount; i++)
            {
                var enemyBall = Instantiate(_enemyBallPrefab, transform);
                enemyBall.gameObject.SetActive(false);
                _enemyBalls.Add(enemyBall);
            }
        }

        protected override void HandleBallActivated(FriendBall ball)
        {
            CheckSpawnEnemies();
        }

        protected override void HandleBlockActivated(Block block)
        {
            CheckSpawnEnemies();
        }


        private void CheckSpawnEnemies()
        {
            var allDropped = _dropElements.TrueForAll(match => match.State == DropState.Dropped);
            if (!allDropped)
            {
                return;
            }

            this.SafeStopCoroutine(_checkSpawnEnemiesRoutine);
            _checkSpawnEnemiesRoutine = StartCoroutine(CheckRoutine());


            IEnumerator CheckRoutine()
            {
                var maxWaitSeconds = 3;
                for (int i = 0; i < maxWaitSeconds; i++)
                {
                    yield return YieldCollection.WaitForSeconds(1);
                    var allStopped = _dropElements.TrueForAll(dropper => !dropper.IsMoving());
                    if (allStopped)
                    {
                        break;
                    }
                }

                StartCoroutine(SpawnEnemiesRoutine());
            }
        }

        private IEnumerator SpawnEnemiesRoutine()
        {
            var wScreenSize = UIManager.WorldScreenSize;
            var topMidScreen = UIManager.WorldScreenCorners[1];
            topMidScreen.x += wScreenSize.x / 2;
            topMidScreen.y += 1;
            foreach (var enemyBall in _enemyBalls)
            {
                var randomPos = topMidScreen;
                randomPos.x += Random.Range(-2f, 2f);
                enemyBall.transform.position = randomPos;
                enemyBall.gameObject.SetActive(true);
                enemyBall.AddRandomForce();
                yield return null;
            }

            var maxWaitSeconds = 5;
            for (int i = 0; i < maxWaitSeconds; i++)
            {
                yield return YieldCollection.WaitForSeconds(1);
                var allHidden = _enemyBalls.TrueForAll(enemyBall => !enemyBall.gameObject.activeInHierarchy);
                if (allHidden)
                {
                    break;
                }
            }
            Win();
        }

    }
}
