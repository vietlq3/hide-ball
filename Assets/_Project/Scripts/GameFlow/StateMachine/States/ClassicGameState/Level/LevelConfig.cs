using System;
using UnityEngine;

namespace Seekers.HideBall.Gameplay.Classic
{
    [CreateAssetMenu(menuName = "Gameplay/Classic/LevelConfig")]
    public class LevelConfig : ScriptableObject
    {
        [SerializeField] private Level[] _levels;

        public int LevelCount => _levels.Length;

        public Level GenerateLevel(int index)
        {
            index = index.ClampCollection(_levels);
            var prefab = _levels[index];
            return Instantiate(prefab);
        }

    }
}