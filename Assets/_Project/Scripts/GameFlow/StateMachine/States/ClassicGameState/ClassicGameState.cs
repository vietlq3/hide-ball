using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Seekers.HideBall.Audio;
using Seekers.HideBall.Gameplay.Classic;
using Seekers.HideBall.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Seekers.HideBall.GameFlowStates
{
    public class ClassicGameState : GameFlowState
    {
        private ClassicGameUI _gameUI;
        private Level _level;
        private int _lastLevelIndex;

        public ClassicGameState(GameFlowStateMachine stateMachine) : base(stateMachine) { }

        protected override void Init()
        {
        }

        public override void Preload()
        {
            UIManager.PreloadUI<ClassicGameUI>();
        }

        protected override void OnEnter(IState lastState)
        {
            AudioManager.FadeCurrentMusic(0.5f);

            _gameUI = UIManager.ShowUI<ClassicGameUI>();
            _gameUI.SetupButtons(NextLevel, () => GenerateLevel(_lastLevelIndex));
            NextLevel();
        }

        private void NextLevel()
        {
            var index = (_lastLevelIndex + 1) % _gameUI.LevelConfig.LevelCount;
            GenerateLevel(index);
        }

        private void GenerateLevel(int index)
        {
            var level = _gameUI.LevelConfig.GenerateLevel(index);
            _lastLevelIndex = index;
            _level = level;
            GameFlow.ClearChild();
            GameFlow.AddChild(level.transform);

            level.OnWin = HandleOnWin;
            level.OnLose = HandleOnLose;
        }

        protected override void OnExit(IState nextState)
        {

        }

        private void HandleOnWin(WinRecap recap)
        {
            UIManager.ShowPopup<MessagePopup>().NotifyGreen("You Win!");
        }

        private async void HandleOnLose(LoseRecap recap)
        {
            if (recap.deathReason != DeathReason.OutOfScreen)
            {
                await UniTask.WaitForSeconds(2);
            }
            UIManager.ShowPopup<MessagePopup>().NotifyRed("You Lose!");
        }

    }
}
