using UnityEngine;

namespace Seekers.HideBall.Gameplay.Classic
{
    public enum DeathReason { TouchedEnemy, OutOfScreen, Cold, Wet }
    public class LoseRecap
    {
        public DeathReason deathReason;
    }
    
    public class WinRecap
    {

    }
}