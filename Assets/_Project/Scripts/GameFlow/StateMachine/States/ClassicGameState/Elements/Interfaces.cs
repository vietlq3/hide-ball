using System.Collections;
using System.Collections.Generic;

namespace Seekers.HideBall.Gameplay.Classic
{
    public enum DropState { Suspended, Dropped }

    public interface IMoveable
    {
        bool IsMoving();
    }

    public interface IDroppable : IMoveable
    {
        DropState State { get; }
    }
}