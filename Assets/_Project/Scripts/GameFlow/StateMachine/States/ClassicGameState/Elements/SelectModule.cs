using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.U2D;

namespace Seekers.HideBall.Gameplay.Classic
{
    [RequireComponent(typeof(Collider2D))]
    public class SelectModule : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        [SerializeField] private GameObject _selection;
        [SerializeField] private bool _persistSelection;

        public Action OnSelect;
        public bool CanSelect { get; set; } = true;

        public void Deselect()
        {
            _selection.SetActive(false);
        }

        [NaughtyAttributes.Button]
        private void UpdateSelection()
        {
            if (TryGetComponent<SpriteShapeController>(out var spriteShapeController))
            {
                var pointCount = spriteShapeController.spline.GetPointCount();
                var selectionSSC = _selection.GetComponent<SpriteShapeController>();
                for (int i = 0; i < pointCount; i++)
                {
                    var point = spriteShapeController.spline.GetPosition(i);
                    selectionSSC.spline.SetPosition(i, point);
                }
            }
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (!CanSelect) { return; }

            _selection.SetActive(true);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if (!_persistSelection)
            {
                Deselect();
            }
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (!CanSelect) { return; }

            OnSelect?.Invoke();
        }
    }
}