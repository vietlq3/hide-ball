using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall.Gameplay.Classic
{
    public class FriendBall : MonoBehaviour
    {
        [SerializeField] private Material _inactiveMat;
        [SerializeField] private Material _activeMat;
        [SerializeField] private Sprite _deadSprite;

        private SpriteRenderer _spriteRenderer;
        private SelectModule _selectModule;
        private DropModule _dropModule;

        public Action OnActivated;
        public Action OnOutOfScreen;
        public Action<Collision2D> OnCollided;
        public Action<GameObject> OnParticleCollided;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _selectModule = GetComponent<SelectModule>();
            _dropModule = GetComponent<DropModule>();

            _selectModule.OnSelect = Activate;
            Deactivate();
        }

        private void OnParticleCollision(GameObject other)
        {
            OnParticleCollided?.Invoke(other);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            OnCollided?.Invoke(other);
        }

        private void OnBecameInvisible()
        {
            var wPos = transform.position;
            var halfWSize = UIManager.WorldScreenSize / 2;

            if (wPos.x < -halfWSize.x || wPos.x > halfWSize.x || wPos.y < -halfWSize.y || wPos.y > halfWSize.y)
            {
                OnOutOfScreen?.Invoke();
            }
        }

        private void Deactivate()
        {
            _dropModule.Suspend();
            _spriteRenderer.material = _inactiveMat;
        }

        private void Activate()
        {
            _dropModule.Drop();
            _spriteRenderer.material = _activeMat;
            _selectModule.CanSelect = false;
            transform.SetAsLastSibling();
            OnActivated?.Invoke();
        }


        public void Kill()
        {
            _spriteRenderer.sprite = _deadSprite;
        }

    }
}
