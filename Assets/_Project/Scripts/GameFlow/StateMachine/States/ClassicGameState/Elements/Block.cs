using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall.Gameplay.Classic
{
    public class Block : MonoBehaviour
    {
        [SerializeField] private Material _inactiveMat;
        [SerializeField] private Material _activeMat;

        private Renderer _renderer;
        private SelectModule _selectModule;
        private DropModule _dropModule;

        public Action OnActivated;

        private void Awake()
        {
            _renderer = GetComponent<Renderer>();
            _selectModule = GetComponent<SelectModule>();
            _dropModule = GetComponent<DropModule>();

            _selectModule.OnSelect = Activate;
            Deactivate();
        }

        private void Deactivate()
        {
            _dropModule.Suspend();
            _renderer.material = _inactiveMat;
        }

        private void Activate()
        {
            _dropModule.Drop();
            _selectModule.CanSelect = false;
            _renderer.material = _activeMat;
            transform.SetAsLastSibling();
            OnActivated?.Invoke();
        }
    }
}
