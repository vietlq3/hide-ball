using UnityEngine;

namespace Seekers.HideBall.Gameplay.Classic
{
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class DropModule : MonoBehaviour, IDroppable
    {
        [SerializeField] private Rigidbody2D _rigidbody;
        [SerializeField] private Collider2D _collider;

        private DropState _state;
        public DropState State => _state;

        private void Reset()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            if (TryGetComponent<CompositeCollider2D>(out var compositeCollider))
            {
                _collider = compositeCollider;
            }
            else
            {
                _collider = GetComponent<Collider2D>();
            }
        }

        public void Suspend()
        {
            _collider.isTrigger = true;
            _rigidbody.bodyType = RigidbodyType2D.Static;
            _state = DropState.Suspended;
        }

        public void Drop()
        {
            _collider.isTrigger = false;
            _rigidbody.bodyType = RigidbodyType2D.Dynamic;
            _state = DropState.Dropped;
        }

        public bool IsMoving()
        {
            return !_rigidbody.IsSleeping();
        }
    }
}