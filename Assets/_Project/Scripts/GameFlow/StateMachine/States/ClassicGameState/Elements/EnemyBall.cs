using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall
{
    public class EnemyBall : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D _rigidbody;

        private bool _isVisible = false;

        private void OnBecameVisible()
        {
            _isVisible = true;
        }

        private void OnBecameInvisible()
        {
            if (!_isVisible) { return; }

            var wPos = transform.position;
            var halfWSize = UIManager.WorldScreenSize / 2;

            if (wPos.x < -halfWSize.x || wPos.x > halfWSize.x || wPos.y < -halfWSize.y || wPos.y > halfWSize.y)
            {
                gameObject.SetActive(false);
            }
        }

        public void AddRandomForce()
        {
            _rigidbody.AddForce(new Vector2(Random.Range(-20f, 20f), Random.Range(0f, -10f)));
        }

    }
}
