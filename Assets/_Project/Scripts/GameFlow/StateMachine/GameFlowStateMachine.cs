using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Seekers.HideBall.GameFlowStates;
using UnityEngine;

namespace Seekers.HideBall
{
    public class GameFlowStateMachine : MonoBehaviourStateMachine<GameFlowState>
    {
        private Coroutine _updateRoutine;

        public GameFlowStateMachine(MonoBehaviour coroutineRunner) : base(coroutineRunner)
        {
            var initState = new InitState(this);
            var mainMenuState = new MainMenuState(this);
            var classicGameState = new ClassicGameState(this);
            var endGameState = new EndGameState(this);

            SetupStates(initState, mainMenuState, classicGameState, endGameState);

            OnStateChange += CheckUpdate;

            ChangeState(initState);
        }

        private void CheckUpdate(IState newState)
        {
            var type = newState.GetType();
            var bindings = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            var methodInfo = type.GetMethod("OnUpdate", bindings);

            var isUpdateMethodOverridden = methodInfo != null && methodInfo.GetBaseDefinition().DeclaringType != methodInfo.DeclaringType;

            if (!isUpdateMethodOverridden)
            {
                CoroutineRunner.StopCoroutine(_updateRoutine);
                return;
            }

            _updateRoutine = CoroutineRunner.StartCoroutine(UpdateRoutine());

            IEnumerator UpdateRoutine()
            {
                while (true)
                {
                    yield return null;
                    StateUpdate();
                }
            }
        }
    }
}