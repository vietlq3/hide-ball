using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.GameFlowStates;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall
{
    public class GameFlow : MonoBehaviour
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        [SerializeField] private GameObject _debugConsolePrefab;
#endif
        private GameFlowStateMachine _stateMachine;
        public static event Action<bool> OnAppPaused;

        private bool _isAppPaused;

        private static GameFlow _instance;


        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            _instance = this;

            var rootGameObjects = gameObject.scene.GetRootGameObjects();
            foreach (var obj in rootGameObjects)
            {
                foreach (var constructable in obj.GetComponentsInChildren<IConstructable>(true))
                {
                    constructable.Construct();
                }
            }

            InitDebug();
        }

        private void OnDestroy()
        {
            _instance = null;
            OnAppPaused = null;
        }

        private void Start()
        {
            _stateMachine = new GameFlowStateMachine(this);
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus != _isAppPaused)
            {
                OnAppPaused?.Invoke(pauseStatus);
            }

            _isAppPaused = pauseStatus;
        }

        private void OnApplicationFocus(bool focusStatus)
        {
            if (focusStatus == _isAppPaused)
            {
                OnAppPaused?.Invoke(!focusStatus);
            }

            _isAppPaused = !focusStatus;
        }

        private void InitDebug()
        {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.unityLogger.logEnabled = true;
            GameObject.Instantiate(_debugConsolePrefab);
#else
            Debug.unityLogger.logEnabled = false;
#endif
        }

        public static void AddChild(Transform transform)
        {
            transform.SetParent(_instance.transform);
        }

        public static void ClearChild()
        {
            _instance.transform.DestroyChildren();
        }

        public static void Restart()
        {
            var fadeSetting = FadeSetting.FadeIn();
            fadeSetting.OnFinish = () => _instance._stateMachine.ChangeState<MainMenuState>();
            UIManager.FadeTransition(fadeSetting);
        }
    }
}