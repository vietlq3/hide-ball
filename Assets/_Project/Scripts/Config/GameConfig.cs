using System;
using System.Collections.Generic;
using UnityEngine;

namespace Constants
{
    public static class GameConfig
    {
        public static class URL
        {
            public const string BundleId = "com.athena.hide.ball";

            public static readonly string IosSettings = $"app-settings:{BundleId}";

            // Taken from https://github.com/FifiTheBulldog/ios-settings-urls/blob/master/settings-urls.md
            public static readonly string IosLocationSettings = $"App-prefs:Privacy&path=LOCATION/{BundleId}";
        }
    }
}