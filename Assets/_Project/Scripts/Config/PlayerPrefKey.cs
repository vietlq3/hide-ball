using System;
using System.Collections.Generic;
using UnityEngine;

namespace Constants
{
    public static class PlayerPrefsKey
    {
        public const string MusicVolume = "Device_MusicVolume";
        public const string SoundVolume = "Device_SoundVolume";
        public const string MuteSound = "Device_MuteSound";
        public const string MuteMusic = "Device_MuteMusic";
        public const string AllowVibration = "Device_AllowVibration";
    }
}