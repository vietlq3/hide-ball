﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Constants
{
    public static class SortingLayerName
    {
        public const string Default = "Default";
        public const string Popup = "Popup";
        public const string OnTop = "OnTop";
    }
}