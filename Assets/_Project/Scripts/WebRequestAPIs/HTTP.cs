using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace WebRequestAPI
{
    public static class HTTP
    {
        /// <summary>  Try-catch <see cref="UnityWebRequestException"/> to get error details </summary>
        public static async UniTask<string> Get(string url, Dictionary<string, string> headers = null)
        {
            using var webRequest = UnityWebRequest.Get(url);
            webRequest.timeout = 10;

            var debugColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            var debugIndicator = "▇ ".Colorize(debugColor);

            Debug.LogWarning(debugIndicator + $"HTTP-GET: {url}\nHeaders: [{headers?.JoinElements(separator: ", ")}]");
            webRequest.SetRequestHeaders(headers);

            try
            {
                await webRequest.SendWebRequest();
            }
            catch (Exception ex)
            {
                Debug.LogError(debugIndicator + $"GET exception from [{url}]: {ex}");
                throw ex;
            }

            Debug.Log(debugIndicator + $"Data from [{url}]:\n{webRequest.downloadHandler.text}");

            return webRequest.downloadHandler.text;
        }

        /// <summary>  Try-catch <see cref="UnityWebRequestException"/> to get error details </summary>
        public static async UniTask<string> Post(string url, string requestBody, Dictionary<string, string> headers)
        {
            using var webRequest = UnityWebRequest.Post(url, requestBody);
            webRequest.timeout = 10;

            var debugColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            var debugIndicator = "▇ ".Colorize(debugColor);

            Debug.LogWarning(debugIndicator + $"HTTP-POST: {url}\nRequest body: {requestBody}\nHeaders: [{headers?.JoinElements(separator: ", ")}]");

            var uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(requestBody));
            if (headers.ContainsValue("application/json"))
            {
                uploadHandler.contentType = "application/json";
            }
            else
            {
                headers.Add("Content-Type", "application/json");
                uploadHandler.contentType = "application/json";
            }

            webRequest.uploadHandler = uploadHandler;
            webRequest.SetRequestHeaders(headers);

            // Catch it like this
            try
            {
                await webRequest.SendWebRequest();
            }
            catch (Exception ex)
            {
                Debug.LogError(debugIndicator + $"POST exception from [{url}]: {ex}");
                throw;
            }

            Debug.Log(debugIndicator + $"Data from [{url}]:\n{webRequest.downloadHandler.text}");

            return webRequest.downloadHandler.text;
        }
    }
}