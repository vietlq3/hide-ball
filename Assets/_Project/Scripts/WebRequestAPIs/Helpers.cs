using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace WebRequestAPI
{
    public static class Extension
    {
        public static void SetRequestHeaders(this UnityWebRequest webRequest, Dictionary<string, string> headers)
        {
            if (headers == null) { return; }
            foreach (var header in headers)
            {
                webRequest.SetRequestHeader(header.Key, header.Value);
            }
        }
    }
}
