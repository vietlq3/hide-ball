using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace WebRequestAPI
{
    public static class GoogleForm
    {
        public static async UniTask Post(string url, Dictionary<string, string> entries)
        {
            var form = new WWWForm();
            foreach (var entry in entries)
            {
                form.AddField(entry.Key, entry.Value);
            }

            using var webRequest = UnityWebRequest.Post(url, form);

            try
            {
                await webRequest.SendWebRequest();
            }
            catch (Exception ex)
            {
                Debug.LogError($"Post Google form failed [{url}]: {ex}");
                throw ex;
            }

            Debug.Log($"Post Google form succeeded [{url}]");
        }

    }
}
