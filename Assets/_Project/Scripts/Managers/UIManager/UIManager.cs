﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Seekers.HideBall.UIManagerControllers;

namespace Seekers.HideBall.UI
{
    public class UIManager : MonoBehaviour, IConstructable
    {
        [SerializeField] private Camera _uiCamera;
        [SerializeField] private EventSystem _eventSystem;
        [SerializeField] private string _uiFolder;
        [SerializeField] private string _popupFolder;
        [SerializeField] private Transform _uiLayer;
        [SerializeField] private AnimationController _animation;

        public static Transform UILayer => _instance._uiLayer;
        public static Camera UICamera => _instance._uiCamera;
        public static EventSystem EventSystem => _instance._eventSystem;
        public static Vector2 WorldScreenSize => _instance?._worldScreenSize ?? default;
        /// <summary> 0 = BottomLeft, 1 = TopLeft, 2 = TopRight, 3 = BottomRight </summary>
        public static Vector3[] WorldScreenCorners => _instance._worldScreenCorners;

        private Vector2 _worldScreenSize;
        private Vector3[] _worldScreenCorners = new Vector3[4];
        private List<BaseUI> _views;

        private Dictionary<string, BaseUI> _instantiatedViews;

        public BaseUI CurrentView => _views.GetLast();
        public BaseUI PreviousView => _views.Count < 2 ? null : _views[_views.Count - 2];

        private static UIManager _instance;

        public const float DefaultTransitionDuration = 0.3f;
        public static bool IsInteractable => EventSystem.gameObject.activeInHierarchy;

        private int _blockCount = 0;

        void IConstructable.Construct()
        {
            if (_instance != null)
            {
                Debug.LogError("You're trying to construct a new instance!");
                return;
            }
            _instance = this;

            var bottomLeft = UICamera.ViewportToWorldPoint(new Vector2(0, 0));
            var topLeft = UICamera.ViewportToWorldPoint(new Vector2(0, 1));
            var topRight = UICamera.ViewportToWorldPoint(new Vector2(1, 1));
            var bottomRight = UICamera.ViewportToWorldPoint(new Vector2(1, 0));

            _worldScreenSize = new Vector2(Vector2.Distance(bottomLeft, bottomRight), Vector2.Distance(bottomLeft, topLeft));
            _worldScreenCorners[0] = bottomLeft;
            _worldScreenCorners[1] = topLeft;
            _worldScreenCorners[2] = topRight;
            _worldScreenCorners[3] = bottomRight;

            _animation.Init(this);

            _instantiatedViews = new Dictionary<string, BaseUI>();

            _views = new List<BaseUI>();

            foreach (var view in _uiLayer.GetComponentsInChildren<BaseUI>(true))
            {
                var viewName = view.GetType().Name;
                _instantiatedViews.Add(viewName, view);
                (view as IElementUI).OnInstantiate();
            }
        }

        private void OnDestroy()
        {
            _instance = null;
        }

#if UNITY_EDITOR
        private List<string> _setInteractableCallers = new();
        [Button]
        private void CheckInteractableCaller()
        {
            Debug.Log(_setInteractableCallers.JoinElements("\n"));
            _setInteractableCallers.Clear();
        }
#endif

        public static void DisableInteractionFor(float durationSeconds)
        {
            SetInteractable(false);
            _instance.WaitAndDo(durationSeconds, () =>
            {
                SetInteractable(true);
            });
        }

        public static void SetInteractable(bool interactable, bool force = false)
        {
#if UNITY_EDITOR
            var collectStackTrace = true;
            if (collectStackTrace)
            {
                try
                {
                    var callerInfo = GeneralUtils.GetMethodCallerInfo();
                    var interactableState = interactable.ToString().Colorize(interactable ? Color.green : Color.red);

                    callerInfo += $", SetInteractable({interactableState}) - [{DateTime.Now:HH:mm:ss.F2}]";

                    _instance._setInteractableCallers.Enqueue(callerInfo);
                    if (_instance._setInteractableCallers.Count > 10)
                    {
                        _instance._setInteractableCallers.Pop();
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogWarning(ex);
                }
            }
#endif

            if (force)
            {
                _instance._blockCount = 0;
                EventSystem.gameObject.SetActive(interactable);
                return;
            }

            if (!interactable)
            {
                EventSystem.gameObject.SetActive(false);
                _instance._blockCount++;
                return;
            }

            _instance._blockCount--;

            if (_instance._blockCount <= 0)
            {
                _instance._blockCount = 0;
                EventSystem.gameObject.SetActive(true);
            }
        }

        public static void ShakeCamera(float duration, float strength = 12, int vibrato = 15)
        {
            _instance._animation.ShakeCamera(duration, strength, vibrato);
        }

        public static YieldInstruction FadeTransition(FadeSetting fadeSetting)
        {
            return _instance._animation.FadeTransition(fadeSetting);
        }

        public static void ShowLoading(bool enable, string text = "")
        {
            _instance._animation.EnableLoading(enable, text);
        }

        public static T PreloadUI<T>(string extraPath = null) where T : BaseUI
        {
            var path = _instance.JoinPaths(_instance._uiFolder, extraPath);
            var view = _instance.GetView(typeof(T).Name, path) as T;
            view.gameObject.SetActive(false);
            return view;
        }

        public static T ShowUI<T>(string extraPath = null) where T : BaseUI
        {
            var path = _instance.JoinPaths(_instance._uiFolder, extraPath);
            return _instance.InternalShow(typeof(T).Name, path) as T;
        }
        public static T ShowPopup<T>(string extraPath = null) where T : Popup
        {
            var path = _instance.JoinPaths(_instance._popupFolder, extraPath);
            return _instance.InternalShow(typeof(T).Name, path) as T;
        }

        private BaseUI InternalShow(string viewName, string path = null)
        {
            var view = GetView(viewName, path);
            if (view == null)
            {
                Debug.LogError("View prefab not found " + viewName);
                return null;
            }

            if (view == CurrentView)
            {
                Debug.LogWarning($"Showing the current view again: [{viewName}]");
                return view;
            }

            _views.Add(view);
            view.gameObject.SetActive(true);
            view.transform.SetAsLastSibling();
            (view as IElementUI).OnShow();

            if (view is Popup popupView)
            {
                switch (popupView.ShowPopupBehaviour)
                {
                    case ShowPopupBehaviour.HideLowerPopup:
                        if (PreviousView is Popup)
                        {
                            Hide(PreviousView);
                        }
                        break;
                }

                if (popupView.ShowPopupBehaviour != ShowPopupBehaviour.DoNothing)
                {
                    _animation.ShowPopupBackgroundDim(DefaultTransitionDuration);
                }
            }

            StartCoroutine(TransitionInRoutine());

            return view;


            IEnumerator TransitionInRoutine()
            {
                SetInteractable(false);
                yield return YieldCollection.WaitForSeconds(view.TransitionInDuration);
                SetInteractable(true);
            }
        }

        public static void HideTopView(Action onFinishTransition = null, bool immediate = false)
        {
            _instance.InternalHide(_instance.CurrentView, onFinishTransition, immediate);
        }

        public static void Hide(BaseUI view, Action onFinishTransition = null, bool immediate = false)
        {
            _instance.InternalHide(view, onFinishTransition, immediate);
        }

        public static void Hide<T>(Action onFinishTransition = null, bool immediate = false) where T : BaseUI
        {
            var viewName = typeof(T).Name;
            if (!_instance._instantiatedViews.TryGetValue(viewName, out var baseView))
            {
                Debug.LogWarning($"View does not exist: {viewName}");
                return;
            }
            _instance.InternalHide(baseView, onFinishTransition, immediate);
        }

        void InternalHide(BaseUI view, Action onFinishTransition, bool immediate = false)
        {
            if (view == null)
            {
                Debug.LogError($"[ViewManager] {view.GetType().Name} is null");
                return;
            };
            if (_views.Count < 2)
            {
#if UNITY_EDITOR
                Debug.LogWarning($"[ViewManager] Trying to hide all UIs, ignore this message if hiding {view.GetType().Name} was intended");
#endif
            }

            _views.Remove(view);

            var noMorePopups = !_views.Exists(match => match is Popup);
            if (noMorePopups) { _animation.HidePopupBackgroundDim(DefaultTransitionDuration); }

            StartCoroutine(TransitionOutRoutine());

            ///
            IEnumerator TransitionOutRoutine()
            {
                (view as IElementUI).OnHide();

                if (!immediate)
                {
                    SetInteractable(false);
                    yield return YieldCollection.WaitForSeconds(view.TransitionOutDuration);
                    SetInteractable(true);
                }

                if (view.DestroyOnHide)
                {
                    if (view != null && view.gameObject != null)
                        Destroy(view.gameObject);
                    _instantiatedViews.Remove(view.GetType().Name);
                }
                else
                {
                    view.gameObject.SetActive(false);
                }
                onFinishTransition?.Invoke();
            }
        }

        private string JoinPaths(params string[] paths)
        {
            var path = string.Join('/', paths);
            for (int i = 0; i < 5; i++)
            {
                if (!path.Contains("//")) { break; }
                path = path.Replace("//", "/");
            }
            return path;
        }

        private BaseUI GetView(string viewName, string path = null)
        {
            if (_instantiatedViews == null) return null;

            if (_instantiatedViews.TryGetValue(viewName, out var baseView))
            {
                return baseView;
            }

            path = JoinPaths(path, viewName);
            var resource = Resources.Load<BaseUI>(path);
            if (resource == null)
            {
                Debug.LogWarning($"Wrong path: {path}");
                if (path.Contains(_uiFolder))
                {
                    path = path.Replace(_uiFolder, _popupFolder);
                }
                else
                {
                    path = path.Replace(_popupFolder, _uiFolder);
                }
                resource = Resources.Load<BaseUI>(path);

                if (resource == null)
                {
                    Debug.LogError($"Cannot find UI in {path}!");
                    return null;
                }

                Debug.LogWarning($"Path was switched to: {path}");
            }

            baseView = Instantiate(resource, _uiLayer);
            (baseView as IElementUI).OnInstantiate();

            _instantiatedViews.Add(viewName, baseView);

            return baseView;
        }


    }
}