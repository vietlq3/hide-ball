using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Seekers.HideBall.UI;

namespace Seekers.HideBall.UIManagerControllers
{
    [Serializable]
    public class AnimationController : CoroutineRunner
    {
        [SerializeField] private Loading _loading;
        [SerializeField] private CanvasGroup _popupBgDim;
        [SerializeField] private SpriteRenderer _screenFaderSprite;
        [SerializeField] private CameraShake _cameraShake;

        private ObjectPool<SpriteRenderer> _faderPool;

        public void Init(MonoBehaviour monoBehaviour)
        {
            _monoBehaviour = monoBehaviour;

            var screenFaderHolder = new GameObject("ScreenFaderHolder").transform;
            screenFaderHolder.SetParent(_screenFaderSprite.transform.parent, false);
            _screenFaderSprite.transform.SetParent(screenFaderHolder, false);

            _faderPool = new ObjectPool<SpriteRenderer>(_screenFaderSprite);
        }

        public void ShakeCamera(float duration, float strength = 12, int vibrato = 15)
        {
            _cameraShake.ShakeCamera(duration, strength, vibrato);
        }

        public Coroutine FadeTransition(FadeSetting fadeSetting)
        {
            return StartCoroutine(FadeRoutine());


            IEnumerator FadeRoutine()
            {
                var faderSprite = _faderPool.Get();
                UIManager.SetInteractable(false);

                if (fadeSetting.FadeInDuration > 0f)
                {
                    faderSprite.SetAlpha(0);
                    yield return faderSprite.DOFade(1, fadeSetting.FadeInDuration).WaitForCompletion();
                }
                else
                {
                    faderSprite.SetAlpha(1);
                }
                fadeSetting.OnFadeInComplete?.Invoke();

                yield return YieldCollection.WaitForSeconds(fadeSetting.WaitAfterFadeIn);
                if (fadeSetting.FadeOutCondition != null)
                {
                    while (!fadeSetting.FadeOutCondition()) { yield return null; }
                }
                fadeSetting.OnFadeOutStart?.Invoke();
                yield return faderSprite.DOFade(0, fadeSetting.FadeOutDuration).WaitForCompletion();

                UIManager.SetInteractable(true);
                fadeSetting.OnFinish?.Invoke();

                _faderPool.Store(faderSprite, false);
            }
        }

        public void ShowPopupBackgroundDim(float fadeInDuration = 0)
        {
            _popupBgDim.DOKill();
            _popupBgDim.gameObject.SetActive(true);
            _popupBgDim.DOFade(1, fadeInDuration);
        }
        public void HidePopupBackgroundDim(float fadeOutDuration = 0)
        {
            _popupBgDim.DOKill();
            _popupBgDim.DOFade(0, fadeOutDuration).OnComplete(() => _popupBgDim.gameObject.SetActive(false));
        }

        public void EnableLoading(bool enable, string text = "")
        {
            _loading.gameObject.SetActive(enable);
            _loading.Text.text = text;
        }
    }
}