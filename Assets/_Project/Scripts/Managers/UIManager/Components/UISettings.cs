using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.UI;
using UnityEngine;

namespace Seekers.HideBall
{
    public enum ShowPopupBehaviour
    {
        DoNothing,
        HideLowerPopup,
    }


    public class FadeSetting
    {
        public float FadeInDuration;
        public float FadeOutDuration;
        public float WaitAfterFadeIn;
        public Action OnFadeInComplete;
        public Action OnFadeOutStart;
        public Action OnFinish;
        public Func<bool> FadeOutCondition;

        public static FadeSetting Default => new FadeSetting(UIManager.DefaultTransitionDuration, UIManager.DefaultTransitionDuration);

        /// <summary> Fade to transparent </summary>
        public static FadeSetting FadeOut(float fadeOutDuration = UIManager.DefaultTransitionDuration)
        {
            var fadeSetting = new FadeSetting(0, fadeOutDuration);
            return fadeSetting;
        }

        /// <summary> Fade to black </summary>
        public static FadeSetting FadeIn(float fadeInDuration = UIManager.DefaultTransitionDuration, float waitAfterFadeIn = 0)
        {
            var fadeSetting = new FadeSetting(fadeInDuration, 0, waitAfterFadeIn);
            return fadeSetting;
        }

        public FadeSetting(float fadeInDuration, float fadeOutDuration, float waitAfterFadeIn = 0)
        {
            FadeInDuration = fadeInDuration;
            FadeOutDuration = fadeOutDuration;
            WaitAfterFadeIn = waitAfterFadeIn;
        }
    }
}