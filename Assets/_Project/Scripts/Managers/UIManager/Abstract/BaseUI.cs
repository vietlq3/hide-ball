using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace Seekers.HideBall.UI
{
    public interface IElementUI
    {
        void OnInstantiate();
        void OnShow();
        void OnHide();
    }

    public abstract class BaseUI : MonoBehaviour, IElementUI
    {
        [field: SerializeField] public bool DestroyOnHide { get; private set; }

        private CanvasGroup _cacheCanvasGroup;
        public CanvasGroup canvasGroup => _cacheCanvasGroup ??= this.ForceGetComponent<CanvasGroup>();
        private UIAnim _cacheUIanim;
        public UIAnim Anim => _cacheUIanim ??= this.ForceGetComponent<UIAnim>();
        public RectTransform rectTransform => transform as RectTransform;

        /// <summary> Used to block user interaction when transitioning </summary>
        public virtual float TransitionInDuration => UIManager.DefaultTransitionDuration;

        /// <summary> Used to block user interaction when transitioning </summary>
        public virtual float TransitionOutDuration => 0;

        /// <summary> Called once only, before OnShow </summary>
        protected virtual void OnInstantiate() { }

        /// <summary> Called every ViewManager.Show() </summary>
        protected virtual void OnShow() { }
        protected virtual void OnHide() { }

        void IElementUI.OnInstantiate() => OnInstantiate();
        void IElementUI.OnShow() => OnShow();
        void IElementUI.OnHide() => OnHide();

        /// <summary> Heavy operation, should not abuse </summary>
        protected Tween FadeInAllSpriteRenderers(float duration = UIManager.DefaultTransitionDuration, bool includeInactive = false)
        {
            var sequence = DOTween.Sequence();
            foreach (var spriteRenderer in GetComponentsInChildren<SpriteRenderer>(includeInactive))
            {
                if (!spriteRenderer.material.HasProperty("_Color")) { continue; }
                sequence.Join(spriteRenderer.material.DOFade(0, duration).From());
            }
            return sequence;
        }
        /// <summary> Heavy operation, should not abuse </summary>
        protected Tween FadeOutAllSpriteRenderers(float duration = UIManager.DefaultTransitionDuration, bool includeInactive = false)
        {
            var sequence = DOTween.Sequence();
            foreach (var spriteRenderer in GetComponentsInChildren<SpriteRenderer>(includeInactive))
            {
                if (!spriteRenderer.material.HasProperty("_Color")) { continue; }
                sequence.Join(spriteRenderer.material.DOFade(0, duration));
            }
            return sequence;
        }

#if UNITY_EDITOR
        [NaughtyAttributes.Button]
        private void OpenInContext()
        {
            if (Application.isPlaying) { return; }
            if (!PrefabUtility.IsPartOfPrefabAsset(this))
            {
                Debug.LogWarning("Please open from Asset");
                return;
            }

            var prefab = PrefabUtility.InstantiatePrefab(this) as BaseUI;
            var parent = GameObject.Find("UIManager/MainCanvas/GameRect/LayerUI").transform;
            prefab.transform.SetParent(parent, false);

            var path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(this);
            PrefabStageUtility.OpenPrefab(path, prefab.gameObject, PrefabStage.Mode.InContext);

            GameObject.DestroyImmediate(prefab.gameObject);
        }
#endif
    }
}