using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall.UI
{
    public class Popup : BaseUI
    {
        [field: SerializeField] public ShowPopupBehaviour ShowPopupBehaviour { get; private set; } = ShowPopupBehaviour.HideLowerPopup;

    }
}
