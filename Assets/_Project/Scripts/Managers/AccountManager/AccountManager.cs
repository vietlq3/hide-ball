using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.Gameplay.Classic;
using UnityEngine;

namespace Seekers.HideBall
{
    public class AccountManager
    {
        private UserProfile _userProfile;
        public static ReadonlyUserProfile UserProfile { get; private set; }

        private static AccountManager _instance;

        private readonly string _path = SaveSystem.GetObscuredPath("UserProfile");

        [RuntimeInitializeOnLoadMethod]
        private static void Init() => _instance = new AccountManager();
        private AccountManager()
        {
            _userProfile = SaveSystem.Encrypted.Load<UserProfile>(_path, SaveSystem.SerializeType.NewtonsoftJson) ?? new UserProfile();
            UserProfile = new ReadonlyUserProfile(_userProfile);

            GameFlow.OnAppPaused -= HandleAppPaused;
            GameFlow.OnAppPaused += HandleAppPaused;
        }

        private void HandleAppPaused(bool paused)
        {
            if (paused)
            {
                SaveProfile();
            }
            else
            {

            }
        }

        private void SaveProfile()
        {
            SaveSystem.Encrypted.Save(_path, NewtonsoftSerializer.ToJson(_userProfile));
        }

        public static void ChangeName(string name)
        {
            _instance._userProfile.UserName = name;
        }


    }
}
