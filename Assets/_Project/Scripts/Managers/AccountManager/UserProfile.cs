using System;
using System.Collections;
using System.Collections.Generic;
using Seekers.HideBall.Gameplay.Classic;
using UnityEngine;

namespace Seekers.HideBall
{
    [Serializable]
    public class UserProfile
    {
        public string UserName;
    }

    public readonly struct ReadonlyUserProfile
    {
        private readonly UserProfile _userProfile;

        public string UserName => _userProfile.UserName;

        public ReadonlyUserProfile(UserProfile userProfile)
        {
            _userProfile = userProfile;
        }
    }
}
