using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Seekers.HideBall.Audio
{
    // [CreateAssetMenu()]
    public class AudioAsset : UniqueScriptableObject<AudioAsset>
    {
        [SerializeField] private MusicCollection _musicCollection;
        [SerializeField] private SoundCollection _soundCollection;

        public static MusicCollection Music => _instance._musicCollection;
        public static SoundCollection Sound => _instance._soundCollection;

#if UNITY_EDITOR
        [Space]
        [SerializeField] private UnityEngine.Object _autoGenFolder;

        [NaughtyAttributes.Button]
        private void AutoSet()
        {
            var audioFolderPath = AssetDatabase.GetAssetPath(_autoGenFolder);
            var rootNode = CSharpGenerator.AudioRefGenerator.Generate(Path.GetFullPath(audioFolderPath));

            var audioGUIDs = AssetDatabase.FindAssets($"t:{nameof(AudioClip)}", new[] { audioFolderPath });

            _musicCollection = new();
            var musicNode = rootNode.Children[0];

            _soundCollection = new();
            var soundNode = rootNode.Children[1];

            SetValueReflection(_musicCollection, musicNode);
            SetValueReflection(_soundCollection, soundNode);

            AssetDatabase.Refresh();


            void SetValueReflection(object obj, CSharpGenerator.Node node)
            {
                var fields = obj.GetType().GetFields();
                for (int i = 0; i < node.Children.Count; i++)
                {
                    var field = fields[i];
                    var fieldNode = node.Children[i];
                    var path = Path.Combine(audioFolderPath, node.Name, fieldNode.Name);
                    if (!fieldNode.IsFolder)
                    {
                        var asset = AssetDatabase.LoadAssetAtPath<AudioClip>(path);
                        field.SetValue(obj, asset);
                    }
                    else
                    {
                        var audioList = new List<AudioClip>();
                        foreach (var itemNode in fieldNode.Children)
                        {
                            var itemPath = Path.Combine(path, itemNode.Name);
                            var asset = AssetDatabase.LoadAssetAtPath<AudioClip>(itemPath);
                            audioList.Add(asset);
                        }
                        field.SetValue(obj, audioList.ToArray());
                    }

                }
            }
        }

#endif

    }
}