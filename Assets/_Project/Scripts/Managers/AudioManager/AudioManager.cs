using System;
using System.Collections;
using System.Collections.Generic;
using Constants;
using UnityEngine;

namespace Seekers.HideBall.Audio
{
    public class AudioManager : MonoBehaviour, IConstructable
    {
        public const float FadeOutDuration = 0.5f;

        [SerializeField] private AudioObject _audioObjSample;


        [NaughtyAttributes.Expandable]
        [SerializeField] private AudioAsset _audioAsset;

        private ObjectPool<AudioObject> _soundPool;
        private AudioObject _musicObj;
        private AudioSource _oneShotSource;

        private static AudioManager _instance;

        public static AudioClip CurrentMusic => _instance._musicObj.source.clip;

        public static float MusicVolume
        {
            get => PlayerPrefs.GetFloat(PlayerPrefsKey.MusicVolume, 1f);
            set
            {
                var volume = Mathf.Clamp01(value);
                PlayerPrefs.SetFloat(PlayerPrefsKey.MusicVolume, volume);
                _instance._musicObj.source.volume = volume;
            }
        }

        public static float SoundVolume
        {
            get => PlayerPrefs.GetFloat(PlayerPrefsKey.SoundVolume, 1f);
            set
            {
                var volume = Mathf.Clamp01(value);
                PlayerPrefs.SetFloat(PlayerPrefsKey.SoundVolume, volume);
                foreach (var audio in _instance._soundPool.ActiveElements)
                {
                    audio.source.volume = volume;
                }
            }
        }

        public static bool MuteMusic
        {
            get => PlayerPrefsExt.GetBool(PlayerPrefsKey.MuteMusic);
            set
            {
                PlayerPrefsExt.SetBool(PlayerPrefsKey.MuteMusic, value);
                _instance._musicObj.source.mute = value;
            }
        }

        public static bool MuteSound
        {
            get => PlayerPrefsExt.GetBool(PlayerPrefsKey.MuteSound);
            set
            {
                PlayerPrefsExt.SetBool(PlayerPrefsKey.MuteSound, value);
                foreach (var audio in _instance._soundPool.ActiveElements)
                {
                    audio.source.mute = value;
                }
            }
        }

        void IConstructable.Construct()
        {
            if (_instance != null)
            {
                Debug.LogError("You're trying to construct a new instance!");
                return;
            }

            _instance = this;

            _oneShotSource = GameObject.Instantiate(_audioObjSample, _audioObjSample.transform.parent).source;
            _oneShotSource.volume = 1;
            _oneShotSource.name = "OneShotSource";

            _soundPool = new ObjectPool<AudioObject>(_audioObjSample);

            _musicObj = GameObject.Instantiate(_audioObjSample, _audioObjSample.transform.parent);
            _musicObj.gameObject.SetActive(true);
            _musicObj.name = "MusicSource";
        }

        public static void PlayMusic(AudioClip music)
        {
            var musicObj = _instance._musicObj;
            if (musicObj.source.isPlaying)
            {
                musicObj.FadeVolume(0, FadeOutDuration, Play);
            }
            else
            {
                Play();
            }


            void Play()
            {
                musicObj.Play(music, true, MusicVolume);
                musicObj.source.mute = MuteMusic;
            }
        }

        public static void FadeCurrentMusic(float to, float duration = 0.5f, Action onComplete = null)
        {
            _instance._musicObj.FadeVolume(to, duration, onComplete);
        }

        public static void StopMusic()
        {
            _instance._musicObj.Stop();
        }

        public static void PauseMusic()
        {
            _instance._musicObj.source.Pause();
        }

        public static void UnpauseMusic()
        {
            _instance._musicObj.source.UnPause();
        }

        /// <summary> One-shot sounds will not be affected by mute or sound volume changes after playing </summary>
        public static void PlaySound(AudioClip sfx, bool oneShot = true)
        {
            if (oneShot && !MuteSound)
            {
                // PlayOneShot() is more performant than Play()
                _instance._oneShotSource.PlayOneShot(sfx, SoundVolume);
                return;
            }

            var soundObj = _instance._soundPool.Get();
            soundObj.Play(sfx, false, SoundVolume);
            soundObj.OnStop = () =>
            {
                _instance._soundPool.Store(soundObj);
            };
            soundObj.source.mute = MuteSound;
        }

        public static void PlaySound(AudioClip sfx, out ExposedAudioObject exposedAudioObject)
        {
            var soundObj = _instance._soundPool.Get();
            var exposed = new ExposedAudioObject(soundObj);

            soundObj.Play(sfx, false, SoundVolume);
            soundObj.OnStop = () =>
            {
                _instance._soundPool.Store(soundObj);
                exposed.Deconstruct();
            };
            soundObj.source.mute = MuteSound;

            exposedAudioObject = exposed;
            return;
        }

    }
}