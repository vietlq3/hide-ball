using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Seekers.HideBall
{
    public class AudioObject : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        public AudioSource source => _audioSource;

        public Action OnStop;

        public void Play(AudioClip clip, bool loop, float volume)
        {
            _audioSource.clip = clip;
            _audioSource.loop = loop;

            _audioSource.DOKill();
            _audioSource.volume = volume;
            _audioSource.Play();

            if (!loop)
            {
                StartCoroutine(CheckStopRoutine());
            }


            IEnumerator CheckStopRoutine()
            {
                var length = clip.length;
                while (_audioSource.time < length)
                {
                    yield return null;
                }
                OnStop?.Invoke();
            }
        }

        public void Stop()
        {
            _audioSource.DOKill();
            StopAllCoroutines();
            _audioSource.Stop();
            OnStop?.Invoke();
        }

        public void FadeVolume(float to, float duration, Action onComplete = null)
        {
            _audioSource.DOFade(to, duration).OnComplete(() => onComplete?.Invoke());
        }
    }

    public class ExposedAudioObject
    {
        private AudioObject _audioObject;
        public event Action OnStop
        {
            add
            {
                _audioObject.OnStop += value;
            }
            remove
            {
                _audioObject.OnStop -= value;
            }
        }

        public ExposedAudioObject(AudioObject audioObject)
        {
            _audioObject = audioObject;
        }

        public void Deconstruct()
        {
            _audioObject = null;
        }

        public void Pause()
        {
            _audioObject.source.Pause();
        }

        public void Unpause()
        {
            _audioObject.source.UnPause();
        }
    }
}
