using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpdateableObject { }
public interface IUpdater : IUpdateableObject { void PerformUpdate(); }
public interface ILateUpdater : IUpdateableObject { void PerformLateUpdate(); }
public interface IFixedUpdater : IUpdateableObject { void PerformFixedUpdate(); }

public class UpdateManager : MonoBehaviour
{
    private static UpdateManager _instance;
    private HashSet<IUpdater> _updaters = new();
    private HashSet<ILateUpdater> _lateUpdaters = new();
    private HashSet<IFixedUpdater> _fixedUpdaters = new();

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
    }

    private void Update()
    {
        foreach (var updater in _updaters)
        {
            updater.PerformUpdate();
        }
    }

    private void LateUpdate()
    {
        foreach (var lateUpdater in _lateUpdaters)
        {
            lateUpdater.PerformLateUpdate();
        }
    }

    private void FixedUpdate()
    {
        foreach (var fixedUpdater in _fixedUpdaters)
        {
            fixedUpdater.PerformFixedUpdate();
        }
    }

    private void OnDestroy()
    {
        _instance = null;
    }

    private static void CheckInit()
    {
        if (_instance == null)
        {
            new GameObject("UpdateManager").AddComponent<UpdateManager>();
        }
    }

    public static void Register(IUpdateableObject updateableObject)
    {
        CheckInit();

        // Use multiple 'if' instead of switch for multiple inheritance

        if (updateableObject is IUpdater updater)
        {
            _instance._updaters.Add(updater);
        }

        if (updateableObject is ILateUpdater lateUpdater)
        {
            _instance._lateUpdaters.Add(lateUpdater);
        }

        if (updateableObject is IFixedUpdater fixedUpdater)
        {
            _instance._fixedUpdaters.Add(fixedUpdater);
        }
    }

    public static void Unregister(IUpdateableObject updateableObject)
    {
        CheckInit();

        // Use multiple 'if' instead of switch for multiple inheritance

        if (updateableObject is IUpdater updater)
        {
            _instance._updaters.Remove(updater);
        }

        if (updateableObject is ILateUpdater lateUpdater)
        {
            _instance._lateUpdaters.Remove(lateUpdater);
        }

        if (updateableObject is IFixedUpdater fixedUpdater)
        {
            _instance._fixedUpdaters.Remove(fixedUpdater);
        }
    }

    // public static void Clear()
    // {
    //     CheckInit();
    //     _instance._updaters.Clear();
    //     _instance._lateUpdaters.Clear();
    //     _instance._fixedUpdaters.Clear();
    // }
}
