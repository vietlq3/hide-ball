using Constants;
using UnityEngine;

public class DeviceManager
{
    public static bool AllowVibration
    {
        get => PlayerPrefsExt.GetBool(PlayerPrefsKey.AllowVibration, true);
        set => PlayerPrefsExt.SetBool(PlayerPrefsKey.AllowVibration, value);
    }

    public static void Vibrate()
    {
        if (AllowVibration)
        {
            Handheld.Vibrate();
        }
    }
}