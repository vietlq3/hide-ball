using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Seekers.HideBall.Gameplay.Classic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Seekers.HideBall.UI
{
    public class ClassicGameUI : BaseUI
    {
        [SerializeField] private LevelConfig _levelConfig;
        [SerializeField] private Transform _playArea;
        [SerializeField] private Button _loadRandomBtn;
        [SerializeField] private Button _restartBtn;

        public LevelConfig LevelConfig => _levelConfig;

        public void SetupButtons(Action onRandom, Action onRestart)
        {
            _loadRandomBtn.SetOnClick(() => onRandom?.Invoke());
            _restartBtn.SetOnClick(() => onRestart?.Invoke());
        }
    }
}
