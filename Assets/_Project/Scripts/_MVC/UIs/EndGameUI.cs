using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIEffects;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Seekers.HideBall.UI
{
    public class EndGameUI : BaseUI
    {
        [SerializeField] private Button _resultBtn, _restartBtn;
        [SerializeField] private Image _bg, _xmas;

        public Action OnClickResult
        {
            set => _resultBtn.SetOnClick(value);
        }

        public Action OnClickRestart
        {
            set => _restartBtn.SetOnClick(value);
        }

        protected override void OnInstantiate()
        {
#if !DEVELOPMENT_BUILD
            _restartBtn.gameObject.SetActive(false);
#endif
        }

        protected override void OnShow()
        {
            PlayEnterAnim();
        }

        private void PlayEnterAnim()
        {
            var bgDissolve = _bg.ForceGetComponent<UIDissolve>();
            bgDissolve.effectFactor = 0;
            bgDissolve.color = Color.black;

            var xmasAnim = _xmas.ForceGetComponent<UIAnim>();

            var sequence = DOTween.Sequence();
            sequence.Append(DOVirtual.Float(1, 0, 1.5f, value =>
            {
                bgDissolve.effectFactor = value;
            }))
            .Join(xmasAnim.PlayAppear(0.5f).SetDelay(0.75f));

        }

    }
}