using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Seekers.HideBall.UI
{
    public class MainMenuUI : BaseUI
    {
        [SerializeField] private PanelController<MainMenuUIPanel> _panelController;

        public PanelController<MainMenuUIPanel> PanelController => _panelController;

        protected override void OnInstantiate()
        {
            _panelController.ForEach(panel => panel.Init());
        }

        protected override void OnShow()
        {
            _panelController.ForEach(panel => panel.gameObject.SetActive(false));
        }
    }
}
