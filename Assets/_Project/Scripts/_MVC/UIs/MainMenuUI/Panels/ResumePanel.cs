using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Seekers.HideBall.UI
{
    public class ResumePanel : MainMenuUIPanel
    {
        [SerializeField] private TextMeshProUGUI _descriptionText;
        [SerializeField] private Button _okBtn;

        public string DescriptionText
        {
            get => _descriptionText.text;
            set => _descriptionText.text = value;
        }

        public Action OnClickOk
        {
            set => _okBtn.SetOnClick(value);
        }

    }
}
