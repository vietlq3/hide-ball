using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Seekers.HideBall.UI
{
    public class CodePanel : MainMenuUIPanel
    {
        [SerializeField] private TMP_InputField _inputField;
        [SerializeField] private TextMeshProUGUI _descriptionText;
        [SerializeField] private Button _okBtn;

        public override void Init()
        {
            _inputField.text = "";
        }

        public override void OnShow()
        {
            StartCoroutine(CoroutineUtils.AutoSelectInputFieldRoutine(_inputField));
        }

        public string InputText
        {
            get => _inputField.text;
        }

        public string DescriptionText
        {
            get => _descriptionText.text;
            set => _descriptionText.text = value;
        }

        public Action OnClickOk
        {
            set => _okBtn.SetOnClick(value);
        }

    }
}
