using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Seekers.HideBall.UI
{
    public class NamePanel : MainMenuUIPanel
    {
        [SerializeField] private TMP_InputField _inputField;
        [SerializeField] private Button _okBtn;

        public override void Init()
        {
            _inputField.text = "";
        }

        public override void OnShow()
        {
            StartCoroutine(CoroutineUtils.AutoSelectInputFieldRoutine(_inputField));
        }

        public string InputText
        {
            get => _inputField.text;
        }

        public int CharacterLimit
        {
            get => _inputField.characterLimit;
            set => _inputField.characterLimit = value;
        }

        public Action OnClickOk
        {
            set => _okBtn.SetOnClick(value);
        }

    }
}
