using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Seekers.HideBall.UI
{
    public class SplashUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI _loadingText;

        private const string LoadingText = "Loading";
        private int _lastDotCount;

        protected override void OnShow()
        {
            StartCoroutine(RunLoading());
        }

        private IEnumerator RunLoading()
        {
            _loadingText.text = LoadingText;
            _loadingText.alignment = TextAlignmentOptions.Left;
            while (true)
            {
                yield return null;

                var dotCount = Mathf.RoundToInt((Time.time % 0.5f) * 10);
                dotCount = Mathf.Clamp(dotCount, 0, 3);

                if (_lastDotCount == dotCount) { continue; }
                _lastDotCount = dotCount;

                _loadingText.text = LoadingText + new string('.', dotCount);
            }
        }

        public void SetText(string text)
        {
            StopAllCoroutines();
            _loadingText.text = text;
            _loadingText.alignment = TextAlignmentOptions.Center;
        }
    }
}
