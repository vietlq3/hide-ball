using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Seekers.HideBall.Audio;

namespace Seekers.HideBall.UI
{
    public class MessagePopup : Popup
    {
        // Left, Right
        public enum ButtonType { None, Red, Green }

        [SerializeField] private TextMeshProUGUI _contentText;
        [SerializeField] private Button _redBtn, _greenBtn;

        private TextMeshProUGUI _redBtnText, _greenBtnText;

        private Action _onClickRedBtn, _onClickGreenBtn;
        public override float TransitionOutDuration => UIManager.DefaultTransitionDuration * 0.6f;

        protected override void OnHide()
        {
            Anim.FadeOut(TransitionOutDuration);
        }

        protected override void OnShow()
        {
            _redBtnText = _redBtn.GetComponentInChildren<TextMeshProUGUI>();
            _greenBtnText = _greenBtn.GetComponentInChildren<TextMeshProUGUI>();

            _redBtn.SetOnClick(Click_RedBtn);
            _greenBtn.SetOnClick(Click_GreenBtn);

            Anim.PlayAppear();

            void Click_RedBtn()
            {
                AudioManager.PlaySound(AudioAsset.Sound.btn_tap);
                _onClickRedBtn?.Invoke();
            }

            void Click_GreenBtn()
            {
                AudioManager.PlaySound(AudioAsset.Sound.btn_tap);
                _onClickGreenBtn?.Invoke();
            }
        }

        public void ManualSetup(Setting setting)
        {
            _contentText.text = setting.Content;
            _redBtnText.text = setting.RedBtnText;
            _greenBtnText.text = setting.GreenBtnText;

            HideBtn(setting.ButtonToHide);
            SetButtonCallbacks(setting.OnClickRedBtn, setting.OnClickGreenBtn, setting.CloseOnClick);
        }

        public void NotifyGreen(string content, string btnText = "OK", Action onClickBtn = null, bool closeOnClick = true)
        {
            ManualSetup(Setting.NotifyGreen(content, btnText, onClickBtn, closeOnClick));
        }

        public void NotifyRed(string content, string btnText = "OK", Action onClickBtn = null, bool closeOnClick = true)
        {
            ManualSetup(Setting.NotifyRed(content, btnText, onClickBtn, closeOnClick));
        }

        public void SetButtonCallbacks(Action onClickRedBtn, Action onClickGreenBtn = null, bool closeOnClick = true)
        {
            if (closeOnClick)
            {
                _onClickRedBtn = () => UIManager.Hide(this);
                _onClickGreenBtn = () => UIManager.Hide(this);
            }
            _onClickRedBtn += onClickRedBtn;
            _onClickGreenBtn += onClickGreenBtn;
        }

        public void MoveButtonToLeft(ButtonType buttonType)
        {
            switch (buttonType)
            {
                case ButtonType.Red:
                    _redBtn.transform.SetAsFirstSibling();
                    break;
                case ButtonType.Green:
                    _greenBtn.transform.SetAsFirstSibling();
                    break;
            }
        }

        public void HideBtn(ButtonType buttonType)
        {
            GetButton(buttonType)?.gameObject.SetActive(false);
        }

        protected Button GetButton(ButtonType buttonType)
        {
            switch (buttonType)
            {
                case ButtonType.Red:
                    return _redBtn;
                case ButtonType.Green:
                    return _greenBtn;
            }
            return null;
        }


        public class Setting
        {
            public string Content;
            public string RedBtnText, GreenBtnText;
            public Action OnClickRedBtn, OnClickGreenBtn;
            public ButtonType ButtonToHide = ButtonType.None;
            public bool CloseOnClick = true;

            public Setting() { }
            public Setting(string content)
            {
                Content = content;
            }

            public static Setting NotifyGreen(string content, string btnText, Action onClickBtn = null, bool closeOnClick = true)
            {
                var setting = new Setting(content)
                {
                    GreenBtnText = btnText,
                    OnClickGreenBtn = onClickBtn,
                    ButtonToHide = ButtonType.Red,
                    CloseOnClick = closeOnClick
                };
                return setting;
            }

            public static Setting NotifyRed(string content, string btnText, Action onClickBtn = null, bool closeOnClick = true)
            {
                var setting = new Setting(content)
                {
                    RedBtnText = btnText,
                    OnClickRedBtn = onClickBtn,
                    ButtonToHide = ButtonType.Green,
                    CloseOnClick = closeOnClick
                };
                return setting;
            }

        }
    }
}