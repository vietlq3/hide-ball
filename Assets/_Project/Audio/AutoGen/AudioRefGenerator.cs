#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace CSharpGenerator
{
    [Serializable]
    public class Node
    {
        public string Name;
        public bool IsFolder;
        public List<Node> Children = new();

        public void AddChildren(string path)
        {
            var dirs = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(dirs))
            {
                // we are adding a file
                var file = Path.GetFileName(path);
                Children.Add(new Node { Name = file, IsFolder = false });
            }
            else
            {
                // we are adding a directory
                var firstDir = dirs.Split(Path.DirectorySeparatorChar)[0];
                var childNode = Children.FirstOrDefault(d => d.Name == firstDir);
                if (childNode == null)
                {
                    childNode = new Node { Name = firstDir, IsFolder = true };
                    Children.Add(childNode);
                }

                var subPath = path.Substring(firstDir.Length + 1);
                childNode.AddChildren(subPath);
            }
        }
    }

    public class AudioRefGenerator
    {
        [MenuItem("Tools/TestAudioRefGenerator")]
        static void Test()
        {
            var rootPath = Path.Combine(Application.dataPath, "_Project/Audio/AutoGen");
            Generate(rootPath);
        }

        public static Node Generate(string rootPath)
        {
            var audioPaths = Directory.GetFiles(rootPath, "*", SearchOption.AllDirectories)
                .Where((fileName) => fileName.EndsWith(".mp3") || fileName.EndsWith(".wav"))
                .Select(fileName => fileName.Replace(rootPath, "").TrimStart(Path.DirectorySeparatorChar))
                .ToList();

            var rootNode = new Node { Name = "AutoGen", IsFolder = true };

            foreach (var path in audioPaths)
            {
                rootNode.AddChildren(path);
            }

            var filePath = Path.Combine(rootPath, "AudioCollection.cs");

            var generator = new ClassGenerator();
            generator.AddLine("using System;");
            generator.AddLine("using UnityEngine;");
            generator.AddLine();
            generator.AddLine($"namespace {typeof(Seekers.HideBall.Audio.AudioAsset).Namespace}");
            generator.AddLine("{");
            foreach (var node in rootNode.Children)
            {
                AddObject(node);
                generator.AddLine();
            }
            generator.AddLine("}");

            generator.CreateFile(filePath);

            Debug.Log($"Generated Audio Ref at {rootPath}");

            return rootNode;


            void AddObject(Node node)
            {
                generator.AddLine("[Serializable]");
                generator.AddLine($"public class {node.Name}Collection");
                generator.AddLine("{");
                foreach (var childNode in node.Children)
                {
                    var name = Path.GetFileNameWithoutExtension(childNode.Name);
                    var invalidName = Regex.IsMatch(name, "[^A-Za-z0-9_]");
                    if (invalidName)
                    {
                        Debug.LogError($"Invalid name: {name}. Name can only contain alphanumeric and '_'");
                        continue;
                    }
                    generator.AddLine($"public AudioClip{(childNode.IsFolder ? "[]" : "")} {name};");
                }
                generator.AddLine("}");
            }
        }
    }

}

#endif