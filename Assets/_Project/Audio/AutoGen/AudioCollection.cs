// Auto-generated by CSharpGenerator.ClassGenerator

using System;
using UnityEngine;

namespace Seekers.HideBall.Audio
{
    [Serializable]
    public class MusicCollection
    {
        public AudioClip christmas;
        public AudioClip end_game;
    }

    [Serializable]
    public class SoundCollection
    {
        public AudioClip btn_tap;
        public AudioClip incorrect;
        public AudioClip[] Correct;
    }

}
