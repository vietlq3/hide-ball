#if UNITY_IOS

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.IO;
using UnityEditor.iOS.Xcode;

public class AegisPostProcess : MonoBehaviour
{
    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string buildPath)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string pbxProj = buildPath + "/Unity-iPhone.xcodeproj/project.pbxproj";

            UpdateProject(pbxProj);
            UpdateProjectPlist(buildPath + "/Info.plist");
            // UpdateCapabilities(pbxProj, buildPath + "/Unity-iPhone/project.entitlements");
            // AddFrameworks(pbxProj, Environment.GetEnvironmentVariable("AEGIS_IOS_FRAMEWORKS"));
        }
    }

    private static void UpdateProject(string projectPath)
    {
        PBXProject project = new PBXProject();
        project.ReadFromString(File.ReadAllText(projectPath));
#if UNITY_2019_3_OR_NEWER
        string targetId = project.GetUnityMainTargetGuid();
#else
        string targetId = project.TargetGuidByName(PBXProject.GetUnityTargetName());
#endif
        project.AddBuildProperty(targetId, "OTHER_LDFLAGS", "-ObjC");
        project.SetBuildProperty(targetId, "CLANG_ENABLE_MODULES", "YES");
        project.SetBuildProperty(targetId, "GCC_ENABLE_OBJC_EXCEPTIONS", "YES");

        // if (System.Environment.GetEnvironmentVariable("AEGIS_BUILD_TYPE") == "release")
        // {
        //     project.SetBuildProperty(targetId, "ENABLE_BITCODE", "NO");
        // }
        // else
        // {
            project.SetBuildProperty(targetId, "ENABLE_BITCODE", "NO");
        // }

        File.WriteAllText(projectPath, project.WriteToString());
    }

    private static void UpdateProjectPlist(string plistPath)
    {
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        // Get root
        PlistElementDict rootDict = plist.root;

        rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);

        // remove exit on suspend if it exists
        // Ref: https://forum.unity.com/threads/the-info-plist-contains-a-key-uiapplicationexitsonsuspend.689200/
        string exitsOnSuspendKey = "UIApplicationExitsOnSuspend";
        if (rootDict.values.ContainsKey(exitsOnSuspendKey))
        {
            rootDict.values.Remove(exitsOnSuspendKey);
        }

        File.WriteAllText(plistPath, plist.WriteToString());
    }

    private static void UpdateCapabilities(string projectPath, string entitlementPath)
    {
        var scheme = "Unity-iPhone";
        var pcm = new ProjectCapabilityManager(projectPath, entitlementPath, scheme);

        pcm.AddInAppPurchase();
        pcm.AddPushNotifications(development: false);
        pcm.AddGameCenter();

        pcm.WriteToFile();
    }

    private static void AddFrameworks(string projectPath, string frameworks)
    {
        if (frameworks == null || frameworks.Equals("none")) return;

        string[] iOSFrameworks = frameworks.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

        if (iOSFrameworks.Length > 0)
        {
            PBXProject project = new PBXProject();
            project.ReadFromString(File.ReadAllText(projectPath));

#if UNITY_2019_3_OR_NEWER
            string targetGUID = project.GetUnityFrameworkTargetGuid();
#else
            string targetGUID = project.TargetGuidByName("Unity-iPhone");
#endif
            foreach (var framework in iOSFrameworks)
            {
                project.AddFrameworkToProject(targetGUID, framework, false);
            }

            File.WriteAllText(projectPath, project.WriteToString());
        }
    }
}

#endif
